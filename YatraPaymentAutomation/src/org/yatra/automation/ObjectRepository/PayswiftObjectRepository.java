package org.yatra.automation.ObjectRepository;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.yatra.automation.Utility.commoOperations;


public class PayswiftObjectRepository extends commoOperations {

	WebDriver driver;	
	Select otherNBList;
	public PayswiftObjectRepository(WebDriver driver2) {
		this.driver = driver2;
	}

	//Initializing all elements present in this page
	public PayswiftObjectRepository  pageObject() throws InterruptedException 
	{
		return (PageFactory.initElements(driver, PayswiftObjectRepository.class));		
	}
	
	public enum PayOp { CREDIT, DEBIT }  
	
	@FindBy(xpath="//span[@id='spanEditStep1']")
	private WebElement editButton;
	
	public void clickEditButton()
	{
		editButton.click();
	}
	
	@FindBy(id="redeem-ecash-button")
	private WebElement redeemNowButton;
	
	public void clickRedeemNowButton()
	{
		try{
		redeemNowButton.click();}
		catch(Exception ex){}
	}
	
	@FindBy(xpath="//div[@id='redeem-block-id']//a")
	private WebElement sliderEcash;
	
	@FindBy(id="total-redeemable-ecash")
	private WebElement totalRedeemableEcash;
	
	public int getRedeemableEcash()
	{
		try{
		return (convertStringToInt(totalRedeemableEcash.getText()));}
		catch(Exception ex){return 0;}
	}
	
	public void moveSlider() throws InterruptedException
	{
		try{
		Actions builder = new Actions(driver);
		Action dragAndDrop = builder.clickAndHold(sliderEcash).moveByOffset(-100, 0).release().build();
	    dragAndDrop.perform();
		}catch(Exception ex)
		{
			
		}
	}
	
	@FindBy(id="ecash-error-message")
	private WebElement ecashErrorMsg;
	
	public boolean isEcashErrorMsgPresent()
	{
		try{
			JavascriptExecutor js =  (JavascriptExecutor) driver;
			js.executeScript("document.getElementById('ecash-error-message').style.display='true';");
			return ecashErrorMsg.isDisplayed();
		}
		catch(Exception ex)
		{
			return false;
		}
	}
	
	@FindBy(id="cancelRedemption")
	private WebElement cancelRedemption; 
	
	public WebElement getCancelRedemptionButton()
	{
		return cancelRedemption;
	}
	
	public void clickCancelRedemption()
	{
		try{
		waitTillElementPresent(driver, cancelRedemption );
		cancelRedemption.click();}
		catch(Exception ex){}
	}
	
	@FindBy(id="available-bal")
	private WebElement availableBalAfterEcashRedemption;
	
	public int getAvailableBalAfterEcashRedemption()
	{
		try{
		waitTillElementPresent(driver, availableBalAfterEcashRedemption);
		return(convertStringToInt(availableBalAfterEcashRedemption.getText()));}
		catch(Exception ex){return 0;}
	}
	@FindBy(id="totalAmountSpan")
	private WebElement totalAmountSpan;
	
	public int getTotalAmount()
	{
		try{
		return (convertStringToInt(totalAmountSpan.getText()));}
		catch(Exception ex){return 0;}
	}
//**************Payment Options**************

	@FindBy(id="cc")
	private WebElement creditCardPayOp;
	
    public void selectCreditCardPayOp()
    {
    	creditCardPayOp.click();
    }
	
	@FindBy(id="dc")
	private WebElement debitCardPayOp ;
	
	public void selectDebitCardPayOp()
    {
		debitCardPayOp.click();
    }

	@FindBy(id="nb")
	private WebElement netBankingPayOp;
	
	public void selectNetBankingPayOp()
    {
		try{
		netBankingPayOp.click();
		}
		catch(Exception ex)
		{
			
		}
    }

	@FindBy(id="emi")
	private WebElement emiPayOp;
	
	public void selectEmiPayOp()
    {
		emiPayOp.click();
    }	
	
	@FindBy(id="mw")
	private WebElement mobileWalletPayOp;
	
	public void selectMobileWalletPayOp()
	{
		mobileWalletPayOp.click();
	}
	
	@FindBy(xpath="//li[@id='cpmt_others']/a")
	private WebElement othersPayOp;
	
	public void selectOtherPayOp(WebDriver driver)
	{
		scrollTillElementIsVisible(othersPayOp, driver);
		othersPayOp.click();
	}	
	
	@FindBy(id="cashCard")
	private WebElement cashCardPayOp;
	public void selectCashCardPayOp()
	{
		cashCardPayOp.click();
	}	
	
	@FindBy(xpath="//input[@type='submit']")
	private WebElement continueOnCashCard;
	
	public void clickContinueOnCashCard(WebDriver driver)
	{
		scrollTillElementIsVisible(continueOnCashCard, driver);
		continueOnCashCard.click();	
	}
	
	@FindBy(xpath="//input[@name='cancel']")
	private WebElement cancelOnCashCard;
	
	public void clickCancelOnCashCard(WebDriver driver)
	{
		scrollTillElementIsVisible(cancelOnCashCard, driver);
		cancelOnCashCard.click();
	}
	@FindBy(id="atm")
	private WebElement atmPayOp;
	public void selectAtmPayOp()
	{
		atmPayOp.click();
	}	
	
	@FindBy(id="ec")
	private WebElement ezeClickPayOp;
	public void selectEzeClickPayOp()
	{
		ezeClickPayOp.click();
	}	
	
	@FindBy(id="rewards")
	private WebElement citiRewardsPayOp;
	public void selectCitiRewardsPayOp()
	{
		citiRewardsPayOp.click();
	}
	

	//************************Quick Book Radio Button for Credit Card and Debit Card************************
	@FindBy(xpath="//li[@class='cc_0']//b[@class='tick']")
	private WebElement ccQuickBookRadioButton;
	
	@FindBy(xpath="//li[@class='dc_0']//b[@class='tick']")
	private WebElement dcQuickBookRadioButton;
	
	@FindBy(xpath="/label[@id='qb_newCreditCard']//b[@class='tick']")
	private WebElement newCreditCard;
	
	@FindBy(xpath="/label[@id='qb_newDebitCard']//b[@class='tick']")
	private WebElement newDebitCard;
	
	@FindBy(id="qb_ccCVV0")
	private WebElement qbCreditCardCVV;
	
	@FindBy(id="qb_dcCVV0")
	private WebElement qbDebitCardCVV;
	
	@FindBy(xpath="//div[@id='tab_dcQB']//i[@class='PaymentSprite removeIcon']")
	private WebElement debitCardQuickBookDeleteButton;
	
	@FindBy(xpath="//div[@id='tab_ccQB']//i[@class='PaymentSprite removeIcon']")
	private WebElement creditCardQuickBookDeleteButton;
	
	//*************************** Credit Card ***********************
	
	@FindBy(id="cc_cno_id")
	private WebElement creditCardNumber;
	
	public void enterCreditCardNumber(String cardNum)
	{
		creditCardNumber.sendKeys("4889940400016536");
	}
	
	@FindBy(id="cc_cardholder_name_id")
	private WebElement creditCardCardHolderName;
	
	public void enterCreditCardCardHolderName(String name)
	{
		creditCardCardHolderName.sendKeys("karishma koul");
	}
	
	@FindBy(id="cc_expm_id")
	private WebElement creditCardExpiryMonth;
	
	public void enterCreditCardExpiryMonth(String expMon)
	{
		Select select = new Select(creditCardExpiryMonth);
		select.selectByValue(expMon);
	}
		
	@FindBy(id="cc_expy_id")
	private WebElement creditCardExpiryYear;
	public void enterCreditCardExpiryYear(String  expYear)
	{
		Select select = new Select(creditCardExpiryYear);
		select.selectByValue(expYear);
	}
	
	@FindBy(id="cc_cvv_id")
	private WebElement newCreditCardCVV;
	
	public void enterCVVForNewCreditCard(String  cvv)
	{
		newCreditCardCVV.sendKeys("773");
	}	
	//**************************Debit Card*********************************
	@FindBy(id="dc_cno_id")
	private WebElement debitCardNumber;
	
	public void enterDebitCardNumber(String cardnum)
	{
		debitCardNumber.sendKeys(cardnum);
	}
	
	@FindBy(id="dc_cardholder_name_id")
	private WebElement debitCardCardHolderName;
	
	public void enterDebitCardCardHolderName(String dcCardHolderName)
	{
		debitCardCardHolderName.sendKeys(dcCardHolderName);
	}
		
	@FindBy(id="dc_expm_id")
	private WebElement debitCardExpiryMonth;
	
	public void enterDebitCardExpiryMonth(String expMonth)
	{
		Select select = new Select(debitCardExpiryMonth);
		select.selectByValue(expMonth);
	}
	
	@FindBy(id="dc_expy_id")
	private WebElement debitCardExpiryYear;
	
	public void enterDebitCardExpiryYear(String expYear)
	{
		Select select = new Select(debitCardExpiryYear);
		select.selectByValue(expYear);
	}
	
	@FindBy(id="dc_cvv_id")
	private WebElement newDebitCardCVV;
	
	public void enterNewDebitCardCVV(String dcCvv)
	{
		newDebitCardCVV.sendKeys(dcCvv);
	}
	
	@FindBy(id="payNow")
	private WebElement payNow;
	
	public void clickPayNow()
	{
		try{
		payNow.click();
		}
		catch(Exception ex){}
	}
	
	@FindBy(xpath ="//input[@class='btn cancel pull-right']")
	private WebElement cancelOnPgMaestro;
	
	@FindBy(linkText="Cancel")
	private WebElement cancelOnPgMaestro2;
	
	public WebElement getcancelOnPgMaestro2()
	{
		try
		{
		return cancelOnPgMaestro2;
		}catch(Exception ex){ return null;}
	}
	public void clickCancelOnPgMaestroAttemptOne()
	{
		try
		{
			if(cancelOnPgMaestro.isDisplayed())
				cancelOnPgMaestro.click();
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void clickCancelOnPgMaestroAttemptTwo()
	{
		try
		{
		    if(cancelOnPgMaestro2.isDisplayed())
				cancelOnPgMaestro2.click();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	//*********************** NetBanking*************************************
	@FindBy(xpath="//div[@class='net-banking-desk']//li[@class='mb20']/label/label")
	private List<WebElement> netBankingOptions;
	
	public String getNetBankingOptionSelected()
	{
		return netBankingOptions.get(7).getAttribute("for");
	}
	public void selectNetBankingOption()
	{
		System.out.println("The net banking option selected is : "+netBankingOptions.get(7).getAttribute("for"));
		netBankingOptions.get(7).click();
	}
	
	@FindBy(id="nprBank")
	private WebElement netBankingBanksDropDown;
		
	//**********************EMI********************************************
	@FindBy(id="emiBank_select")
	private WebElement selectEmiBank;
	
	public void selectEMIBank()
	{
		Select select = new Select(selectEmiBank);
		select.selectByValue("hdfc");
	}
	
	@FindBy(xpath="//label[@for='TncAgree']//span[@class='box']")
	private WebElement termsAndConditionCheckBoxEmi;
	
	public void selectTermsAndConditionCheckBoxEmi()
	{
		termsAndConditionCheckBoxEmi.click();
	}
	
	@FindBy(xpath="//label[@for='hdfc-3']//b")
	private WebElement emiTenureRadioButton;
	
	public void clickEmiTenureRadioButton()
	{
		emiTenureRadioButton.click();
	}
	
	@FindBy(id="emi_cno_id")
	private WebElement emiCradNumberId;
	
	public void enterEmiCradNumberId(String dcCardnum)
	{
		emiCradNumberId.sendKeys(dcCardnum);
	}
	
	@FindBy(id="emi_cardholder_name_id")
	private WebElement emiCradHolderName;
	public void enterEmiCradHolderName(String emiCardHolderName)
	{
		emiCradHolderName.sendKeys(emiCardHolderName);
	}
	
	@FindBy(id="emi_expm_id")
	private WebElement emiExpiryMonth;
	
	public void selectEmiExpiryMonth(String expMonth)
	{
		Select select = new Select(emiExpiryMonth);
		select.selectByValue(expMonth);
	}
	
	@FindBy(id="emi_expy_id")
	private WebElement emiExpiryYear;
	public void selectEmiExpiryYear(String expYear)
	{
		Select select = new Select(emiExpiryYear);
		select.selectByValue(expYear);
	}
	
	@FindBy(id="emi_cvv_id")
	private WebElement emiCVV;
	public void enterEmiCVV(String cvv)
	{
		emiCVV.sendKeys(cvv);
	}
	
	//****************************Mobile Wallet*****************************
	
	@FindBy(xpath="//div[@id='tab_mw']//label/label")
	private List<WebElement> mobileWallets;
	
	public void selectMobilewalletOption()
	{
		mobileWallets.get(3).click(); 
	}
	
	public String getMobileWalletSelected()
	{
		System.out.println("Mobile wallet selected is : "+ mobileWallets.get(3).getAttribute("for") );
		return mobileWallets.get(3).getAttribute("for");
	}
	
	public int getTotalWalletAvailable()
	{
		return mobileWallets.size();
	}
	
	public List<WebElement> getListOfMobileWalletByName()
	{
		return mobileWallets;
	}
	
	//*********************************MobileWallet/NetBanking/Atm card *******************************
	
	@FindBy(xpath="//div[@class='net-banking-desk']//label/label")
	private List<WebElement> listOfNetBankingOptions;
	
	public List<WebElement> getMajorNetBankingOptionsList()
	{
		return listOfNetBankingOptions;
	}
	
	public int getTotalMajotNetBankingOptionsAvailable()
	{
		return listOfNetBankingOptions.size();
	}
	
	@FindBy( xpath="//select[@id='nprBank']")
	private WebElement otherNetBankingOptions;
	
	public WebElement getOtherNetBankingOptions()
	{
		return otherNetBankingOptions;
	}
	
	public int getTotalOtherNetBankingOptionsAvailable()
	{
		return getListOfOtherNetBankingOptionsByName().length;
	}
	
	public String[] getListOfOtherNetBankingOptionsByName()
	{
		otherNBList = new Select(otherNetBankingOptions);	
		int i=1;
		List<String> arr = new ArrayList<String>();
		while(i<(otherNBList.getOptions().size()-1))
		{
			arr.add(otherNBList.getOptions().get(i).getText());		
			i++;
		}		
		return arr.toArray(new String[arr.size()]);
	}
	
	public void selectOtherNBOptionByName(String otherNetBankingName)
	{
		try{
		otherNBList =  new Select(otherNetBankingOptions);
		otherNBList.selectByVisibleText(otherNetBankingName);
		}catch(Exception ex){}
	}
	
	
	@FindBy(xpath="//div[@id='tab_mw']//label/label")
	private List<WebElement> listOfMobileWalletOptions;
	
	
	public void selectSubOptionPayOp(WebElement el)
	{
		try{
		el.click();}catch(Exception ex){}
	}
	
	public boolean isBankPageWorkingFine(String suboptionPayopName)
	{
	  try{
		switch (suboptionPayopName){
		case "allahabad-net":
			return allahabadNetBanking.isDisplayed();			
		case "axis-net" :
			return axisNetBanking.isDisplayed();
		case "citi-net" :
			return citiNetBanking.isDisplayed();
		case "hdfc-net":
			return hdfcNetBanking.isDisplayed();
		case  "icici-net" :
			return iciciNetBanking.isDisplayed();
		case "idbi-net" :
			return  idbiNetBanking.isDisplayed();
		case "kotak-net" :
			return kotakNetBanking.isDisplayed();
		case "pnb-net"	:
			return pnbNetBanking.isDisplayed();
		case "sbi-net" :
			return sbiNetBanking1.isDisplayed();
		case "Andhra Bank" :
			return andhraNetBanking.isDisplayed();
		case "BANK OF BAHRAIN AND KUWAIT" :
			return bbkIndiaNetBanking.isDisplayed();
		case "BANK OF BARODA CORPORATE" :
			return bankOfBarodaCorporateNetBanking.isDisplayed();
		case "BANK OF BARODA RETAIL" :
			return bankOfBarodaRetailNetBanking.isDisplayed();
		case "Bank Of India":
			return bankOfIndiaNetBanking.isDisplayed();
		case "Bank Of Maharashtra":
			return bankOfMaharastraNetBanking.isDisplayed();
		case "Bassien Catholic Co-Operative Bank" :
			return basseinCatholicNetBanking.isDisplayed();
		case "Bhartiya Mahila Bank" :
			return bharatiyaMahilaBankNetBanking.isDisplayed();
		case "Canara Bank" :
			return canaraNetBanking.isDisplayed();
		case "Catholic Syrian Bank" :
			return catholicNetBanking.isDisplayed();
		case "Central Bank Of India" :
			return cantralBankOfIndiaNetBanking.isDisplayed();
		case "City Union Bank" :
			return cityUnionBankNetBanking.isDisplayed();
		case "Corporation Bank" :
			return corporationBankNetBanking.isDisplayed();
		case "DBS Bank Ltd" :
			return dbsBankNetBanking.isDisplayed();
		case "Dena Bank" :
			return denaNetBanking.isDisplayed();
		case "Deutsche Bank" :
			return deutscheNatBanking.isDisplayed();
		case "Development Credit Bank" :
			return dcbNatBanking.isDisplayed();
		case "Development credit Bank - Corporate":
			return dcbNatBanking.isDisplayed();
		case "Dhanlaxmi Bank" :
			return dhanLakshmiNatBanking.isDisplayed();
		case "Digi Bank by DBS" :
			return digiBankNatBanking.isDisplayed();
		case "Federal Bank" :
			return fedralNetBanking.isDisplayed();
		case "IDFC Bank":
			return idfcNetBanking.isDisplayed();
		case "Indian Bank":
		    return indianBankNetBanking.isDisplayed();
		case "Indian Overseas Bank" :
			return indianOverseasNetBanking.isDisplayed();
		case "IndusInd" :
			return InduslandNetBanking.isDisplayed();	
		case "mobikwik" :
			return mobikwikMobilwWallet.isDisplayed();
		case "oxigen" :
			return oxygenMobilwWallet.isDisplayed();
		case "payu":
		    return payuMobilwWallet.isDisplayed();
		case "reliancejio":
			return  joiMobileWallet.isDisplayed();
		case "freecharge" :
			return  freechargeMobileWallet.isDisplayed();
		case "olamoney" :		
			return  olaMobilwWallet.isDisplayed();
		case "masterpass" :
			return  masterPassMobileWallet.isDisplayed();
		case "payzapp" :
			return  payZappMobileWallet.isDisplayed();
		case "voda-mpesa" :
			return  vodaphoneMPesaMobileWallet.isDisplayed();
		case "idea-money" :
			return ideaMoneyWallet.isDisplayed();
		case "Jammu & Kashmir Bank":
			return jandkNetBanking.isDisplayed();
		case "Karnataka Bank" :
			return karnatakaNetBanking.isDisplayed();
		case "Karur Vysya Bank":
		    return karurVysyaNetBanking.isDisplayed();
		    
		case "Lakshmi Vilas Bank":
			return lakshmiVilasNetBanking.isDisplayed();
			
		case "Lakshmi Vilas Retail Banking" :
		    return lakshmiVilasCorporateNetBanking.isDisplayed();
		case "Oriental Bank Of Commerce" :
			return orientalNetBanking.isDisplayed();
			
		case "Punjab & Maharashtra Bank" :
			return punjabAndMaharastraNetBanking.isDisplayed();
			
		case "Punjab National Bank Corporate" :
		    return punjabAndMaharastraNetBanking.isDisplayed();
		    		
		case "Saraswat Bank":
			return saraswatNetBanking.isDisplayed();
			
		case "Shamrao Vithal Bank":
			return shamraoVittalNetBanking.isDisplayed();
		
		case "South Indian Bank" :
			return false;
		case "Standard Chartered Bank" :
			standardChatteredNetBanking.isDisplayed();
			
		case "State Bank Of Bikaner And Jaipur" :
		      return sbiNetBanking1.isDisplayed();
		    		  
		case "State Bank Of Hyderabad" :
			return sbiNetBanking1.isDisplayed();
			
		case "State Bank Of Mysore" :
			return sbiNetBanking1.isDisplayed();
		case "State Bank Of Patiala" :
			return sbiNetBanking1.isDisplayed();
		case "State Bank Of Travancore" :
			return sbiNetBanking1.isDisplayed();
		case "Syndicate Bank":
			return syndicateNetBanking.isDisplayed();
			
		case "Tamilnad Mercantile Bank":
			return false;
					
		case "Tamilnadu Cooperative Bank":
			return false;
		case "UCO Bank":
			return ucoNetBanking.isDisplayed();
		case "Union Bank Of India" :
			return unionBankOfIndiaNetBanking.isDisplayed();
		case "United Bank Of India" :
			return unitedBanloFIndiaNetBanking.isDisplayed();
		case "Vijaya Bank":
			return vijayaNetBanking.isDisplayed();
		case "Yes Bank":
			return yesNetBanking.isDisplayed();
		default:
			return false;
		}}catch(Exception ex){return false;}
	}
	
	@FindBy(css="#virtualpassword")
	private WebElement ucoNetBanking;
	
	
	@FindBy(css=".button")
	private WebElement unionBankOfIndiaNetBanking;
	
	@FindBy(css="#button1")
	private WebElement unitedBanloFIndiaNetBanking;
	
	@FindBy(css="#VALIDATE_CREDENTIALS")
	private WebElement vijayaNetBanking;
	
	@FindBy(css="#Image1")
	private WebElement yesNetBanking;
	
	
	@FindBy(css=".buttsignin")
	private WebElement syndicateNetBanking;
	
	@FindBy(css="#username")
	private WebElement sbiNetBanking1;
	
	@FindBy(css=".button>input")
	private WebElement standardChatteredNetBanking;
	
	@FindBy(css="#ctl00_cphPG_uctLogin_btnContinue")
	private WebElement shamraoVittalNetBanking;
	
	@FindBy(css="#toggleElement")
	private WebElement saraswatNetBanking;
	
	@FindBy(css="#preLoginSubmit")
	private WebElement punjabAndMaharastraNetBanking;
	
	@FindBy(css=".button2")
	private WebElement lakshmiVilasNetBanking, lakshmiVilasCorporateNetBanking;
	
	@FindBy(css=".buttsignin")
	private WebElement karurVysyaNetBanking;
	
	@FindBy(css="#STU_VALIDATE_CREDENTIALS")			
	private WebElement karnatakaNetBanking, orientalNetBanking;
	
	@FindBy(css=".labelColumn")
	private WebElement jandkNetBanking;
	
	@FindBy(xpath="//input[@class='login']")
	private WebElement allahabadNetBanking;
	
	@FindBy(xpath="//span/input[@class='inner-btn cm-button-red']")
	private WebElement axisNetBanking;
	
	@FindBys( {
		 @FindBy(id="proceedForm"),
		 @FindBy(css = "#header")
		} )	
	private WebElement citiNetBanking;
	
	@FindBy(xpath="//input[@class='input_password']")
	private WebElement hdfcNetBanking;
	
	@FindBy(id="VALIDATE_CREDENTIALS1")
	private WebElement iciciNetBanking, fedralNetBanking;
	
	@FindBy(xpath="//input[@name='Action.ShoppingMall.Signon']")
	private WebElement idbiNetBanking;
	
	@FindBy(xpath="//a[@id='secure-login01']")
	private WebElement kotakNetBanking;

	@FindBy(xpath="//input[@class='Button1 button_class'][@title='Login']")
	private WebElement sbiNetBanking;

	@FindBy(id="STU_VALIDATE_CREDENTIALS")
	private WebElement pnbNetBanking;
	
	@FindBy(xpath="//input[@id='button1']")
	private WebElement andhraNetBanking, bbkIndiaNetBanking;	
	
	@FindBy(xpath="//input[@src='Retail.gif']")
	private WebElement bankOfBarodaCorporateNetBanking, bankOfBarodaRetailNetBanking;
	
	@FindBy(xpath="//a/b[contains(text(),'Retail Internet Banking Users - Please click here to PAY')]")
	private WebElement bankOfIndiaNetBanking;
	
	@FindBy(xpath="//a/img[@alt='Login Retail (New IB)']")
	private WebElement bankOfMaharastraNetBanking;
	
	@FindBy(xpath="//input[@id='B1']")
	private WebElement basseinCatholicNetBanking;
	
	@FindBy(xpath="//div[@id='alignMainDiv']")
	private WebElement bharatiyaMahilaBankNetBanking;
	
	@FindBy(xpath="//input[@class='buttsignin']")
	private WebElement canaraNetBanking;
	
	@FindBy(xpath="//div[@class='usrIdCont']")
	private WebElement catholicNetBanking;	

	@FindBy(xpath="//input[@id='uid']")
	private WebElement cantralBankOfIndiaNetBanking;
	
	@FindBy(xpath="//div[@id='NetBanking']")
	private WebElement 	cityUnionBankNetBanking;
	
	@FindBy(xpath="//a[@href='javascript:RetailLogin();']")
	private WebElement 	corporationBankNetBanking;
	
	@FindBy(xpath="//input[@id='formbutton'][@name='Submit']")
	private WebElement 	dbsBankNetBanking;
	
	@FindBy(xpath="//input[@name='RESET']")
	private WebElement 	denaNetBanking;
	
	@FindBy(css="#btnsetPassword")
	private WebElement deutscheNatBanking;
	
	@FindBy(css="#button1")
	private WebElement dcbNatBanking;
	
	@FindBy(css="#onclicksubmit")
	private WebElement dhanLakshmiNatBanking;
	
	@FindBy(css="#frmLogin_logintextBox")	
	private WebElement digiBankNatBanking;
	
	@FindBy(css=".btn.btn-sm.btn-primary.margin-top-30.btn-submit-txnlogin.ng-scope")	
	private WebElement idfcNetBanking;
	
	@FindBy(xpath="//input[@name='LoginButton']")
	private WebElement indianBankNetBanking;
	
	@FindBy(xpath="//a[contains(text(),'Individual Login')]")
	private WebElement indianOverseasNetBanking;
	
	@FindBy(css="#submit")	
	private WebElement InduslandNetBanking;
	
	@FindBy(css="#signinFrm")	
	private WebElement mobikwikMobilwWallet;
	
	@FindBy(css="#BtnLogin")	
	private WebElement oxygenMobilwWallet;
	
	@FindBy(css=".mdm-btn.left")	
	private WebElement payuMobilwWallet;
	
	@FindBy(css=".logojio")	
	private WebElement joiMobileWallet;
	
	@FindBy(css="#signInButton")	
	private WebElement freechargeMobileWallet;
	
	@FindBy(css=".mdl-button.mdl-js-button.mdl-button--raised.mdl-button--colored.w-100.block-center")	
	private WebElement olaMobilwWallet;
	
	@FindBy(css="#walletSearch")	
	private WebElement masterPassMobileWallet;
	
	@FindBy(css="#registerButton")	
	private WebElement payZappMobileWallet;
	
	@FindBy(css="#validateMrchntmsisdn")	
	private WebElement vodaphoneMPesaMobileWallet;
	
	@FindBy(css="#Checkregister")
	private WebElement ideaMoneyWallet;
	
	
	//**************************PG Page Cancel button********************
	
	@FindBy(id="cmdSubmit")
	private WebElement cancelButtonOnPG;
	
	public void clickCancelButtonOnPG()
	{
		try
		{
			cancelButtonOnPG.click();		
		}
		catch(Exception ex)
		{
			
		}
	}
	
	//*******************************ATM****************************
	
	@FindBy(xpath="//div[@id='tab_atm']//label/label")
	private List<WebElement> atmRadioButtons;
	
	public int getTotalAtmPayOpSuboptionsAvailable()
	{
		return atmRadioButtons.size();
	}
	
	public String getAtmSuboptionName(int index)
	{
		return atmRadioButtons.get(index).getAttribute("class");
	}
	
	public WebElement getAtmoptionByIndex(int index)
	{
		return atmRadioButtons.get(index);
	}
	@FindBy(id="CancelBtn")
	private WebElement cancelButton;
	
	public void clickCancelButtonForATM()
	{
	    try
	    {
	    	cancelButton.click();
	    }
	    catch(Exception ex)
	    {
	    	ex.printStackTrace();
	    }
	}
	
	//*******************************EzeClick*******************************
	
	@FindBy(id="authNQuickpayID")
	private WebElement authNQuickpayID;
	
	public boolean isAuthNQuickpayIDEnabled()
	{
		return authNQuickpayID.isDisplayed();
	}
	
	//*******************************Rewards *******************************
	
	@FindBy(id="rw_cno_id")
	private WebElement rewardsCreditCardNum;
	
	public void enterRewardsCreditCardNum(String cardNum)
	{
		rewardsCreditCardNum.sendKeys(cardNum);
	}
	
	@FindBy(id="rw_cardholder_name_id")
	private WebElement rewardsCreditCardName;
	
	public void enterRewardsCreditCardName(String name)
	{
		rewardsCreditCardName.sendKeys(name);
	}
	
	@FindBy(id="rw_expm_id")
	private WebElement rewardExpMonth;
	
	public void selectRewardsExpMon(String expMon)
	{
		Select select = new Select(rewardExpMonth);
		select.selectByValue(expMon);
		
	}
	
	@FindBy(id="rw_expy_id")
	private WebElement rewardExpYear;
	
	public void selectRewardsExpYear(String expYear)
	{
		Select select = new Select(rewardExpYear);
		select.selectByValue(expYear);		
	}
	
	@FindBy(id="rw_cvv_id")
	private WebElement rewardsCvv;
	
	public void enterRewardsCreditCardCvv(String cvv)
	{
		rewardsCvv.sendKeys(cvv);
	}
	
	@FindBy(id="rw_reward_points_id")
	private WebElement rewardsPoints;
	
	public void enterRewardsPonits(String points)
	{
		rewardsPoints.sendKeys(points);
	}
	
	@FindBy(xpath="//li/a[@title='CANCEL']")
	private WebElement cancelRewards;
	
	public boolean isCancelButtonPresent()
	{
		return cancelRewards.isDisplayed();
	}
	
	
	
	//****************************Return Page SAPG***************************
	
	@FindBy(id="panel_container")
	private WebElement panelContainer;
	
	public boolean isReturnURLTrue()
	{
		if(panelContainer.isDisplayed())
			return true;
		else
			return false;
	}
	
	//****************************All Payments Sub options*************************
	
	
	
	
}

