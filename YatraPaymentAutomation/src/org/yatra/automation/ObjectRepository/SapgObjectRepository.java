package org.yatra.automation.ObjectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SapgObjectRepository
{
	
	WebDriver driver;
	
	public SapgObjectRepository(WebDriver driver2)
	{
		this.driver = driver2;	
	}

	//Initializing all elements present in this page
    public SapgObjectRepository pageObject() throws InterruptedException 
	{
		return (PageFactory.initElements(driver, SapgObjectRepository.class));	
    }
	
	@FindBy(xpath="//input[@id='cust_email']")
	private static WebElement custEmail;
	
	@FindBy(id="super_pnr")
	private WebElement super_pnr;
	
	@FindBy(id="loginCheck")
	private WebElement loginCheck;
	
	@FindBy(id="password")
	private WebElement password;
	
	@FindBy(id="login-button")
	private WebElement loginButton;
	
	@FindBy(id="panel_errorDiv")
	private WebElement errorPanel;
	
	public boolean ifRefernceIdAlreadyUsed()
	{
		try
		{
		return errorPanel.isDisplayed();
		}
		catch(Exception ex)
		{
			return false;
		}
	}
	
	public void sapgClickLoginButton()
	{
		loginButton.click();
	}
	
	
	public void sapgCustEmail(String email) throws InterruptedException
	{
		try{
		custEmail.click();
		custEmail.clear();
		custEmail.sendKeys(email);
		}
		catch(Exception ex){}
	}
	
	public void sapgCustPassword(String passwrd)
	{
		try{
		password.clear();
		password.sendKeys(passwrd);
		}
		catch(Exception ex){}
	}
	
	public void checkIHaveAccCheckBox()
	{
		try{
		if(!loginCheck.isSelected())
		{
			loginCheck.click();
		}
		}
		catch(Exception ex){}
	}
	
	public void sapgSuperPnr(String superPnr)
	{
		try{
	 	super_pnr.clear();
		super_pnr.sendKeys(superPnr);
		}
		catch(Exception ex){}
	}
	
	public void loginOnSapg(String email, String password, String ytRefNo) throws InterruptedException
	{
		  sapgCustEmail(email);
		  checkIHaveAccCheckBox();
		 sapgCustPassword(password);
		  sapgSuperPnr(ytRefNo);
		  sapgClickLoginButton();
	}
	
	
	
	
	
	

}
