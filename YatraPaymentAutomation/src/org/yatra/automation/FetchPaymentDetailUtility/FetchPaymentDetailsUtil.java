package org.yatra.automation.FetchPaymentDetailUtility;

import java.util.LinkedList;
import java.util.List;
import org.yatra.automation.Utility.ExcelUtility;
import org.yatra.automation.Utility.FileUtil;
import org.yatra.automation.Utility.JsonUtil;
import org.yatra.automation.Utility.RollingLogFile;
import org.yatra.automation.Utility.SQLUtil;
import org.yatra.automation.propertyAndConfig.ConfigProperties;	

/**
 * @class FectPaymentDetailUtil
 * @description Returns the payment details of top 5 super pnr of an LOB and certain PG
 * @author karishma.koul
 *
 */
public class FetchPaymentDetailsUtil {

	static FetchPaymentDetailsUtil paymentUtil = new FetchPaymentDetailsUtil();
	ExcelUtility excelUtil = new ExcelUtility();
	static final int counter=1;
	FileUtil fileutility= new FileUtil();
	SQLUtil sql =new SQLUtil();	
	LinkedList<String> listOfTtid , listOfSuperPnr= new LinkedList<String>();	
	String listOfPG [], arrLobNameAndPG[] ;
	ConfigProperties config= new ConfigProperties("PROD_FILE");
	JsonUtil jsonutil= new JsonUtil();
	RollingLogFile logger= new RollingLogFile();
	static int y=0;
	
	public static void main(String[] args) throws Exception 
	{
	    int totalRows =paymentUtil.excelUtil.getTotalRowInSheet();
		for(int counter=1;counter<=totalRows;counter++)
		{
			//Reads Product_ui name and payment gateway names in a String array			
			paymentUtil.arrLobNameAndPG = paymentUtil.excelUtil.readxlsfile
					(counter, ConstantUtil.productIdcolumnNum, ConstantUtil.paymentGatewaycolumnNum);
			
			//Since Payment gateway can be comma separated therefore get list of PG using parseCommaSeparatedString()
			paymentUtil.listOfPG=paymentUtil.fileutility.
					parseCommaSeparatedString(paymentUtil.arrLobNameAndPG[1]);
			
			// From PGs and product_ui get the list of top 5 ttids by executing query
			paymentUtil.listOfTtid =paymentUtil.sql.getTopFiveSuperPnrByLobNameAndPG
					(paymentUtil.config.fetchConfig("DB"),
					paymentUtil.arrLobNameAndPG[0], paymentUtil.listOfPG[0],paymentUtil.listOfPG[1]);
			
			//Now we need super pnr from ttid only when product_ui is not STANDALON
			if(!paymentUtil.ifProductIsStandAlon())
			{
				for(int i=0;i<paymentUtil.listOfTtid.size();i++)
				{
					paymentUtil.listOfSuperPnr.add(paymentUtil.fileutility.returnSuperPnrFromTtid
							(paymentUtil.listOfTtid.get(i)));
				 
				}
			}
			
			paymentUtil.getPaymentInfoLOBWise(paymentUtil.listOfTtid, paymentUtil.listOfSuperPnr);
			
		}
	}
	
	/**
	 * Verifies if the product_ui/LOB is StandAlone or not.
	 * Return true if StandAlon else false
	 * @return
	 */
	public boolean ifProductIsStandAlon()
	{
		if(arrLobNameAndPG!=null){
		if(paymentUtil.arrLobNameAndPG[0].equals("STAND_ALON"))
			return true;
		else
		    return false;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Runs the PaymentDetails API and give back the response of API in String format
	 * @param ttid
	 * @param superpnr
	 * @throws Exception
	 */
	public void getPaymentInfoLOBWise(List<String> ttid, List<String>  superpnr) throws Exception
	{
		if(ttid.size()!=superpnr.size())
		{
			System.out.println("There is a mismatch in number of super pnr and ttid for LOB. Please check manually");
		}
		else
		{ for(int x=0;x<ttid.size();x++)
			{				
			 RollingLogFile.createLoggerTextFile(this.getClass().getSimpleName(), "*ttid is : " + ttid.get(x)
			 + "  and PG is : " + paymentUtil.listOfPG[y]);
			 String res=jsonutil.getApiResponse("http://172.16.1.20/checkout/pay/paymentDetails?ttid="
						+ttid.get(x)+"&superPnr="+superpnr.get(x));
			System.out.println(res);
			 RollingLogFile.createLoggerTextFile(this.getClass().getSimpleName(), "**************************************************************************"
			 		+ "***************************");
			 
			}
		}
		y++;
			
	}
	
	
	
	

}
