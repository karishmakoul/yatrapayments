package org.yatra.automation.FetchPaymentDetailUtility;

public class ConstantUtil
{
   public static final int productIdcolumnNum = 0;
   public static final int paymentGatewaycolumnNum = 1;
   public static final String filePath=System.getProperty("user.dir");
   public static final String fileName="DataInput.xlsx";  
   public static final String sheetName="productIdAndPG";
   
}
