package org.yatra.automation.paymentsAPI;



import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class getStoredJsonObject {
	
	@JsonProperty("saveCard")
	private String  saveCard;	
	public String getSavedCard()
	{
		return saveCard;		
	}
	

}
