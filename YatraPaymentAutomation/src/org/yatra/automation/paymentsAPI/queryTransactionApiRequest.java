package org.yatra.automation.paymentsAPI;

import java.util.HashMap;
import java.util.Map;

import org.yatra.automation.propertyAndConfig.ConfigProperties;

public class queryTransactionApiRequest {
	ConfigProperties config= new ConfigProperties("PROD_FILE");
	
	 private Map<String, String> map= new HashMap<String, String>();
	 
	 public queryTransactionApiRequest()
	 {
		 map.put("ttid", config.fetchConfig("ttid"));
		 	 		 
	 }
	 
	 public HashMap<String, String> getMapperForQueryTransactionAPI()
	 {
		 return (HashMap<String, String>) map;
	 }	 
	 public String getApiUrl()
	 {
		 return config.fetchConfig("queryTransactionApi"); 
	 }
}
