package org.yatra.automation.paymentsAPI;
import java.util.HashMap;
import java.util.Map;

import org.yatra.automation.propertyAndConfig.ConfigProperties;


public class paymentRequestAPI {
	
	 ConfigProperties config= new ConfigProperties("PROD_FILE");
	
	 private Map<String, String> map= new HashMap<String, String>();
	 
	 public paymentRequestAPI()
	 {
		 map.put("super_pnr", config.fetchConfig("super_pnr"));
		 map.put("productCode", config.fetchConfig("productCode"));
		 map.put("merchantCode", config.fetchConfig("merchantCode"));
		 map.put("ssoToken", config.fetchConfig("ssoToken"));
		 map.put("client", config.fetchConfig("client"));
		 map.put("uuid", config.fetchConfig("uuid")); 
		 
	 }
	 
	 public HashMap<String, String> getMapperForPaymentRequest()
	 {
		 return (HashMap<String, String>) map;
	 }
	 
	 public String getApiUrl()
	 {
		 return config.fetchConfig("apiUrl"); 
	 }
	 
}
