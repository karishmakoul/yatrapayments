package org.yatra.automation.ECashApi;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetEcashResponse 
{
	@JsonProperty("status")
	private String status;	
	
	public String getStatus()
	{
		return status;
	}
	
	@JsonProperty("responseMsg")
	private String responseMsg;
	
	public String getResponseMsg()
	{
		return responseMsg;
	}
	
	@JsonProperty("totalEcash")
	private GetTotalEcashDto TotalEcashDto;
	
	public GetTotalEcashDto getTotalEcashDto()
	{
		return TotalEcashDto;
	}
	
	@JsonProperty("ECashList")
	private List<GetEcashListDto> ecashList;
	
	public List<GetEcashListDto> getEcashListDto()
	{
		return ecashList;
	}
	
	
	
}
