package org.yatra.automation.ECashApi;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RedeemAuthCallResponse {

	@JsonProperty("status")
	private String status;	
	
	public String getStatus()
	{
		return status;
	}
	
	
	@JsonProperty("responseCode")
	private String responseCode;	
	
	public String getresponseCode()
	{
		return responseCode;
	}
	
	@JsonProperty("responseMsg")
	private String responseMsg;	
	
	public String getResponseMsg()
	{
		return responseMsg;
	}
	
	@JsonProperty("transactionId")
	private String transactionId;	
	
	public String getTransactionId()
	{
		return transactionId;
	}
	
	
	@JsonProperty("walletId")
	private String walletId;	
	
	public String getWalletId()
	{
		return walletId;
	}
	
	@JsonProperty("amountInPaisa")
	private String amountInPaisa;	
	
	public String getAmountInPaisa()
	{
		return walletId;
	}
	
}
