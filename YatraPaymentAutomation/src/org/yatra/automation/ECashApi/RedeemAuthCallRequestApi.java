package org.yatra.automation.ECashApi;

import java.util.HashMap;
import java.util.LinkedHashMap;
import org.yatra.automation.Utility.ApiRequestParser;
import org.yatra.automation.propertyAndConfig.ConfigProperties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public  class RedeemAuthCallRequestApi 
{
	LinkedHashMap<String, String> mapperString = new LinkedHashMap<String, String>();
	
	 ConfigProperties config= new ConfigProperties("PROD_FILE");
	 public RedeemAuthCallRequestApi()
	 {
		 mapperString.put("emailId", config.fetchConfig("emailId"));
		 mapperString.put("transactionId", config.fetchConfig("transactionId"));
		 mapperString.put("activity", config.fetchConfig("activity"));
		 mapperString.put("ecash", config.fetchConfig("ecash"));
		 mapperString.put("ylpMax", config.fetchConfig("ylpMax"));
		 mapperString.put("tenantId", config.fetchConfig("tenantId"));
	 }
	
	 public HashMap<String, String> getMapperForApi()
	 {
		 return (HashMap<String, String>) mapperString;
	 }
	 
	 public String getApiUrl()
	 {
		 return config.fetchConfig("authapi"); 
	 }
	
}
