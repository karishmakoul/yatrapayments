package org.yatra.automation.ECashApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTotalEcashDto {

	@JsonProperty("currentECashInPaisa")
	private String currentECashInPaisa;	
	
	public String getCurrentECashInPaisa()
	{
		return currentECashInPaisa;
	}
	
	@JsonProperty("redeemableECashInPaisa")
	private String redeemableECashInPaisa;	
	
	public String getRedeemableECashInPaisa()
	{
		return currentECashInPaisa;
	}
	
	
	@JsonProperty("redeemedECashInPaisa")
	private String redeemedECashInPaisa;	
	
	public String getRedeemedECashInPaisa()
	{
		return currentECashInPaisa;
	}
	
}
