package org.yatra.automation.ECashApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetEcashListDto
{
	
	@JsonProperty("eCashType")
	private String eCashType;	
	
	public String getECashType()
	{
		return eCashType;
	}
				
	@JsonProperty("currentECashInPaisa")
	private String currentECashInPaisa;	
	
	public String getCurrentECashInPaisa()
	{
		return currentECashInPaisa;
	}
	
	@JsonProperty("redeemableECashInPaisa")
	private String redeemableECashInPaisa;	
	
	public String getRedeemableECashInPaisa()
	{
		return redeemableECashInPaisa;
	}
	
	@JsonProperty("redeemedECashInPaisa")
	private String redeemedECashInPaisa;	
	
	public String getRedeemedECashInPaisa()
	{
		return redeemedECashInPaisa;
	}


	
	 
	
}
