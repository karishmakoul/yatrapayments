package org.yatra.automation.ECashApi;

import java.util.HashMap;
import java.util.Map;

import org.yatra.automation.propertyAndConfig.ConfigProperties;

public class GetEcashRequest 
{
	 ConfigProperties config= new ConfigProperties("PROD_FILE");
		
	 private Map<String, String> map= new HashMap<String, String>();
	 
	 public GetEcashRequest()
	 {
		 map.put("emailId", config.fetchConfig("emailId"));			 
	 }
	
	 public HashMap<String, String> getMapperForGetEcashRequest()
	 {
		 return (HashMap<String, String>) map;
	 }
	 
	 public String getApiUrl()
	 {
		 return config.fetchConfig("getEcashAPI"); 
	 }
}
