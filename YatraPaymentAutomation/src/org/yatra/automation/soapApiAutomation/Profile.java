package org.yatra.automation.soapApiAutomation;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Profile")
public class Profile {

	String profileType;

	@XmlAttribute
	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	
	@XmlElement
	Customer cust= new Customer("yatratestbookings@gmail.com");
	
	
}
