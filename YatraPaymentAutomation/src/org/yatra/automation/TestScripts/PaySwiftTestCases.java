package org.yatra.automation.TestScripts;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
//import org.apache.log4j.spi.LoggerFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.asserts.SoftAssert;
import org.yatra.automation.BrowserInvokeUtilities.BrowserInvoke;
import org.yatra.automation.ObjectRepository.PayswiftObjectRepository;
import org.yatra.automation.ObjectRepository.SapgObjectRepository;
import org.yatra.automation.Utility.ReportGenerator;
import org.yatra.automation.Utility.RollingLogFile;
import org.yatra.automation.Utility.SQLUtil;
import org.yatra.automation.Utility.commoOperations;
import org.yatra.automation.propertyAndConfig.ConfigProperties;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class PaySwiftTestCases {
	 WebDriver driver;
   	 ConfigProperties config= new ConfigProperties("PROD_FILE");
	 PayswiftObjectRepository payswift ;
	 SapgObjectRepository sapg;
	 BrowserInvoke invokebrowser;
	 commoOperations commonOps;
	 SQLUtil sqlutil;
	 List<String> primaryPg, secondaryPg; 
	 String pg2 = null;
	 String  queryForGettingPrimaryPG=null, queryForGettingSecondaryPG=null,queryForGettingPGinReqTable=null;
	// Logger logger = LoggerFactory.getLogger(PaySwiftTestCases.class);
	 SoftAssert s_assert = new SoftAssert();
	 static LinkedHashMap<String, String> testCaseNameAndResult ,screenshotAndOrderId, subOptionsPayopNameAndResult;
	 ReportGenerator extentReport;
	 static int superCounter=0; 
	 public String super_Pnr = "No Super Pnr", super_pnr= null;
	 public static ExtentReports report;
	 public ExtentTest test;
	 ITestResult testResult2;
	 RollingLogFile logger;
	 
  @BeforeClass(groups={"TestAllNBAndWalletOptions"})
   public void beforeClass() {	  
	  try
	  {  	  
		  extentReport= new ReportGenerator();
		  driver=BrowserInvoke.invokeBrowser("chrome");
		  payswift =new PayswiftObjectRepository(driver).pageObject();
		  sapg= new SapgObjectRepository(driver).pageObject();
		  sqlutil = new SQLUtil();
		  invokebrowser= new BrowserInvoke();
		  commonOps= new commoOperations();
		  testCaseNameAndResult = new LinkedHashMap<String, String>();
		  screenshotAndOrderId = new LinkedHashMap<String, String>();
		  subOptionsPayopNameAndResult= new LinkedHashMap<String, String>();
		  report=new ExtentReports(System.getProperty("user.dir")+"\\ExtentReportAPI.html",true);
		  report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		  logger= new RollingLogFile();
	  }
	  catch(Exception ex)
	  {
		ex.printStackTrace();
	  }
  }
  
  @BeforeMethod(groups={"TestAllNBAndWalletOptions"})
  public void beforeMethod(Method m) throws Exception 
  {
	  superCounter++;
	  commonOps.acceptAlert(driver);    // Purpose : In case while navigating to login page from bank page a pop up appears.
	  driver.get(config.fetchConfig("baseUrl"));
	  driver.manage().deleteAllCookies();
	  commonOps.acceptAlert(driver);
	  Thread.sleep(5000);
	// Thread.sleep(5000);
	  sapg.loginOnSapg(config.fetchConfig("custEmail"), config.fetchConfig("passwordSapg"), 
			  config.fetchConfig("superPNR"));
	  Thread.sleep(5000);
	  //commonOps.waitForPageLoad(driver);
	  commonOps.waitTillElementPresent(driver, payswift.getCancelRedemptionButton());
	
	  if(sapg.ifRefernceIdAlreadyUsed())
	  {
		  s_assert.fail("Please create or use fresh allocation id");
		 
		  RollingLogFile.createLoggerTextFile("PaySwiftTestCases",
				  config.fetchConfig("superPNR") +" YT Ref no doesn't have any allocation. "
				  		+ "Please create or use fresh allocation id");
	  }
	 Thread.sleep(5000);	
	  super_pnr=commonOps.getSuperPnr(driver);
	  super_Pnr=commonOps.getSuperPnr(driver)+"_"+superCounter;
	  test = report.startTest(m.getName().toString() );
	  
  }
      
  @Test(priority=1, description="Ecash Redemption and cancel ecash redemption", groups ={"TestEcash"})
  public void verifyEcashRedemption() throws InterruptedException
  {
	RollingLogFile.createLoggerTextFile("PaySwiftTestCases", "**************Entered in verifyEcashRedemption TestCase"
			+ "*****************");
	int finalAmount= payswift.getTotalAmount();
	if(finalAmount==1)
	{
		s_assert.fail("Amount is Re.1. Therefore, ecash slider cannot ebe tested ");
		 RollingLogFile.createLoggerTextFile("PaySwiftTestCases", "Amount in "+
				  config.fetchConfig("superPNR") +" YT Ref no is Re. 1. Therefore, ecash slider cannot ebe tested ");
	}
	else
	{
		payswift.moveSlider();
		int redeemableAmount = payswift.getRedeemableEcash();
		payswift.clickRedeemNowButton();
		s_assert.assertEquals(payswift.isEcashErrorMsgPresent(), false,
				"Error appeared in redeeming Ecash");	
		
		s_assert.assertEquals((finalAmount-redeemableAmount) == 
				payswift.getAvailableBalAfterEcashRedemption(), true, "Amount left after reducing redeemable amount from "
						+ "Final amount is not equal to amount after ecash redemption ");
		
		payswift.clickCancelRedemption();
	    s_assert.assertEquals(finalAmount==(payswift.getTotalAmount()), true,"Amount after reverse auth of ecash redemption is"
	    		+ " not equal to original amount");
	}
	 s_assert.assertAll();
	 RollingLogFile.createLoggerTextFile("PaySwiftTestCases", "**************VerifyEcashRedemption TestCase Ended"
				+ "*****************");
  }
  
  @Test(priority=2 )
  public void verifyCreditCardTransaction() throws InterruptedException
  {
	  RollingLogFile.createLoggerTextFile("PaySwiftTestCases", "**************Entered in verifyCreditCardTransaction TestCase"
				+ "*****************");
	  payswift.enterCreditCardNumber(config.fetchConfig("cardNum"));
	  payswift.enterCreditCardCardHolderName(config.fetchConfig("cardHolderName"));
	  payswift.enterCreditCardExpiryMonth(config.fetchConfig("expMonth"));
	  payswift.enterCreditCardExpiryYear(config.fetchConfig("expYear"));
	  payswift.enterCVVForNewCreditCard(config.fetchConfig("cvv"));
	  payswift.clickPayNow();
	 Thread.sleep(5000);
	  payswift.clickCancelButtonOnPG();
	  commonOps.acceptAlert(driver);
	 Thread.sleep(5000);
	 
	  if(!payswift.isReturnURLTrue())
	  {
		  s_assert.fail("Return url is not coming");  
		  RollingLogFile.createLoggerTextFile("PaySwiftTestCases","Return url is not coming correctly"); 
	  }
	  s_assert.assertAll();
	  RollingLogFile.createLoggerTextFile("PaySwiftTestCases", "**************verifyCreditCardTransaction TestCase Ended"
				+ "*****************");
   }
  
 @Test(priority=3)
  public void verifyMaestroCardWithCVV() throws InterruptedException
  {
	  payswift.selectDebitCardPayOp();
	  payswift.enterDebitCardNumber(config.fetchConfig("meastroCardNum"));
	  payswift.enterDebitCardCardHolderName(config.fetchConfig("cardHolderName"));
	  payswift.enterDebitCardExpiryMonth(config.fetchConfig("expMonth"));
	  payswift.enterDebitCardExpiryYear(config.fetchConfig("expYear"));
	  payswift.enterNewDebitCardCVV(config.fetchConfig("cvv"));
	  payswift.clickPayNow();
	 Thread.sleep(5000);
	  payswift.clickCancelOnPgMaestroAttemptOne();
	  commonOps.acceptAlert(driver);
	  commonOps.waitTillElementPresent(driver, payswift.getcancelOnPgMaestro2());
	  payswift.clickCancelOnPgMaestroAttemptTwo();
	  commonOps.acceptAlert(driver);
	 Thread.sleep(5000);
	  s_assert.assertEquals(payswift.isReturnURLTrue(), true, "Return url is not coming correctly");  
	  s_assert.assertAll();
	  	  
  }
  
  @Test(priority=4)
  public void verifyMaestroWithoutCVV() throws InterruptedException
  {
	  payswift.selectDebitCardPayOp();
	  payswift.enterDebitCardNumber(config.fetchConfig("meastroCardNum"));
	  payswift.enterDebitCardCardHolderName(config.fetchConfig("cardHolderName"));
	  payswift.clickPayNow();
	 Thread.sleep(5000);
	  payswift.clickCancelOnPgMaestroAttemptOne();
	  commonOps.acceptAlert(driver);
	 Thread.sleep(5000);
	  commonOps.waitTillElementPresent(driver, payswift.getcancelOnPgMaestro2());
	  payswift.clickCancelOnPgMaestroAttemptTwo();
	  commonOps.acceptAlert(driver);
	 Thread.sleep(5000);
	  s_assert.assertEquals(payswift.isReturnURLTrue(), true, "Return url is not coming correctly"); 
	  s_assert.assertAll();
	 	  
  }
  
  @Test(priority=5)
  public void verifyNetBankingSingleOption() throws Exception
  {	  
	  payswift.selectNetBankingPayOp();
	  payswift.selectNetBankingOption();
	  
	  primaryPg= sqlutil.getGatewayNameForABank(config.fetchConfig("DB"),payswift.getNetBankingOptionSelected(), "primary_gateway"  );
	  secondaryPg=sqlutil.getGatewayNameForABank(config.fetchConfig("DB"),payswift.getNetBankingOptionSelected(),"secondary_gateway"  );
	 
	  payswift.clickPayNow();
	  		
	  pg2= sqlutil.getPGFromRequestAuditTable(config.fetchConfig("DB"),super_pnr);
	  
	 Thread.sleep(5000);
	  
	  if(!(primaryPg.get(0).equalsIgnoreCase(pg2)||secondaryPg.get(0).equalsIgnoreCase(pg2)))
	  {s_assert.fail("Payment gateways are different in request table and banks master table or "
	  		+ "reutrn url is not correct");
	  RollingLogFile.createLoggerTextFile("PaySwiftTestCases","Payment gateways are different in request table and banks master table or "
		  		+ "reutrn url is not correct");}
	  s_assert.assertAll();
 }
  
  @Test(priority=6)
  public void verifyEmiOptions() throws InterruptedException
  {
	  payswift.selectEmiPayOp();
	  payswift.selectEMIBank();
	  Thread.sleep(3000);
	  payswift.clickEmiTenureRadioButton();
	  payswift.selectTermsAndConditionCheckBoxEmi();
	  payswift.enterEmiCradNumberId(config.fetchConfig("cardNum"));
	  payswift.enterEmiCradHolderName(config.fetchConfig("cardHolderName"));
	  payswift.selectEmiExpiryMonth(config.fetchConfig("expMonth"));
	  payswift.selectEmiExpiryYear(config.fetchConfig("expYear"));
	  payswift.enterEmiCVV(config.fetchConfig("cvv"));
	  payswift.clickPayNow();
	  payswift.clickCancelButtonOnPG();
	  commonOps.acceptAlert(driver);
	 Thread.sleep(5000);
	  s_assert.assertFalse(payswift.isReturnURLTrue(), "Return URL is incorrect");
	  s_assert.assertAll();
  }
  
  @Test(priority=7)
  public void verifyMobileWaletSigleOption() throws Exception
  {
	  payswift.selectMobileWalletPayOp(); 
	  payswift.selectMobilewalletOption();
	  
	  primaryPg= sqlutil.getGatewayNameForABank(config.fetchConfig("DB"),
			  payswift.getMobileWalletSelected(), "primary_gateway");
	  
	  secondaryPg=sqlutil.getGatewayNameForABank(config.fetchConfig("DB"),
			  payswift.getMobileWalletSelected(),"secondary_gateway");
	 
	  payswift.clickPayNow();
	  		
	  pg2= sqlutil.getPGFromRequestAuditTable(config.fetchConfig("DB"),super_pnr);
	  
	 Thread.sleep(5000);
	  
	  if(!(primaryPg.get(0).equalsIgnoreCase(pg2)||secondaryPg.get(0).equalsIgnoreCase(pg2)))
	  s_assert.fail("Payment gateways are different in request table and banks master table or "
	  		+ "reutrn url is not correct"); 
	  s_assert.assertAll();
  }
  
  @Test(priority=8)
  public void verifyCashCard() throws InterruptedException
  {
	  payswift.selectCashCardPayOp();
	  payswift.clickPayNow();
	 Thread.sleep(5000);
	  payswift.clickContinueOnCashCard(driver);
	 Thread.sleep(5000);
	  payswift.clickCancelOnCashCard(driver);
	 Thread.sleep(5000);
	  s_assert.assertFalse(!payswift.isReturnURLTrue(), "Return url is not correct"); 
	  s_assert.assertAll();
  }
  
  @Test(priority=9)
  public void verifyAtmAllOptions() throws InterruptedException
  {
	  payswift.selectAtmPayOp();
	  int   totalAtmOptions = payswift.getTotalAtmPayOpSuboptionsAvailable();
	  for(int x=0; x< totalAtmOptions;x++)
	  {	
		  payswift.selectSubOptionPayOp(payswift.getAtmoptionByIndex(x));
		  driver.wait(3000);
		  payswift.clickPayNow();
		 Thread.sleep(5000);
		  driver.get(config.fetchConfig("baseUrl"));
		 Thread.sleep(5000);
		  sapg.sapgSuperPnr(config.fetchConfig("superPNR"));
		  sapg.sapgClickLoginButton();
		 Thread.sleep(5000);
		  payswift.selectAtmPayOp(); 
	  }
	  s_assert.assertAll();	  
  }
  
  @Test(priority=10)
  public void VerifyEzeClick() throws InterruptedException
  {
	  payswift.selectOtherPayOp(driver);	  
	  payswift.selectEzeClickPayOp();
	  payswift.clickPayNow();
	 Thread.sleep(5000);
	  s_assert.assertFalse(!payswift.isAuthNQuickpayIDEnabled(), "EzeClick Page is not opened");
	  s_assert.assertAll();
	  
  }
  
  @Test(priority=11)
  public void VerifyRewards() throws InterruptedException
  {
	  payswift.selectOtherPayOp(driver);
	  payswift.selectCitiRewardsPayOp();
	  payswift.enterRewardsCreditCardNum(config.fetchConfig("rewardsCardNum"));
	  payswift.enterRewardsCreditCardName(config.fetchConfig("cardHolderName"));
	  payswift.selectRewardsExpMon(config.fetchConfig("rewrdsExpMon"));
	  payswift.selectRewardsExpYear(config.fetchConfig("rewardsExpYear"));
	  payswift.enterRewardsCreditCardCvv(config.fetchConfig("rewardsCvv"));
	  payswift.enterRewardsPonits(config.fetchConfig("rewardsPoints"));
	  payswift.clickPayNow();
	 Thread.sleep(5000);
	  s_assert.assertFalse(!payswift.isCancelButtonPresent(), "Rewards Page is not opened");	
	  commonOps.acceptAlert(driver);
	  s_assert.assertAll();
  }   
  
  @Test(priority= 12, groups={"TestAllNBAndWalletOptions"}) 
  public void verifyAllMobileWalletOptions() throws InterruptedException
  { 
	  System.out.println("***********************************************************************");
	  System.out.println("In verifyAllMobileWalletOptions method");
	  payswift.selectMobileWalletPayOp(); 
	  int totalMobileWallets= payswift.getTotalWalletAvailable();	 	  
	  for(int x=0; x< totalMobileWallets;x++)
	  {		  
		  String mw= payswift.getListOfMobileWalletByName().get(x).getAttribute("class");
		  payswift.selectSubOptionPayOp(payswift.getListOfMobileWalletByName().get(x));
		  payswift.clickPayNow();
		 Thread.sleep(5000);
		  s_assert.assertFalse(payswift.isBankPageWorkingFine(mw),  mw+" page is not working  fine");	 
		  driver.get(config.fetchConfig("baseUrl"));
		  commonOps.acceptAlert(driver);
		 Thread.sleep(5000);
		  sapg.loginOnSapg(config.fetchConfig("custEmail"), config.fetchConfig("passwordSapg"), 
				  config.fetchConfig("superPNR"));
		 Thread.sleep(5000);
		  payswift.selectMobileWalletPayOp(); 
	  }
	  s_assert.assertAll();
  }  
  
  @Test(priority=13, groups={"TestAllNBAndWalletOptions"})
  public void verifyPaymentGatewayOfMajorNetBankingSubOptions() throws WebDriverException, Exception
  {
	
	  payswift.selectNetBankingPayOp();
	  int majorNetBankingOptions= payswift.getTotalMajotNetBankingOptionsAvailable();	 
	  
	  RollingLogFile.createLoggerTextFile(this.getClass().getSimpleName(),"In verifyPaymentGatewayOfMajorNetBankingSubOptions " );
	  for(int x=0; x< majorNetBankingOptions;x++)
	    {
		  payswift.selectSubOptionPayOp(payswift.getMajorNetBankingOptionsList().get(x));
		  payswift.clickPayNow();
		  Thread.sleep(5000);
	
		  if((sqlutil.getPGFromRequestAuditTable(config.fetchConfig("DB"),super_pnr))==config.fetchConfig("paymentGateway"))
		  {
				subOptionsPayopNameAndResult.put(payswift.getMajorNetBankingOptionsList().get(x).getAttribute("class"), 
						(sqlutil.getPGFromRequestAuditTable(config.fetchConfig("DB"),super_pnr)));		 
		  }
		  driver.get(config.fetchConfig("baseUrl"));
		  commonOps.acceptAlert(driver);
		  Thread.sleep(5000);
	
		  sapg.loginOnSapg(config.fetchConfig("custEmail"), config.fetchConfig("passwordSapg"), 
				  config.fetchConfig("superPNR"));
		  Thread.sleep(5000);
		
		  payswift.selectNetBankingPayOp(); 
	  }	  
	  for(Entry<String, String> nn : subOptionsPayopNameAndResult.entrySet())
	  {
		  System.out.println("List of Banks which are on ccavenueDirect" + nn.getKey().toString()+" and "+ nn.getValue().toString());
	  }	   
  }
  
  @Test(priority=14, groups={"TestAllNBAndWalletOptions"})
  public void verifyPaymentGatewayOfOtherNetBankingSubOptions() throws WebDriverException, Exception
  {
	  System.out.println("***********************************************************************");
	  System.out.println("In verifyPaymentGatewayOfOtherNetBankingSubOptions method");
	  payswift.selectNetBankingPayOp();
	  int  netBankingOtherOptions = payswift.getTotalOtherNetBankingOptionsAvailable();
	  String[] netBankingOtherOption = payswift.getListOfOtherNetBankingOptionsByName();
	  for(int y=1;y<netBankingOtherOptions;y++)
		{			
		    payswift.selectOtherNBOptionByName(netBankingOtherOption[y]);
		    payswift.clickPayNow();
			commonOps.waitForPageLoad(driver);
		    if((sqlutil.getPGFromRequestAuditTable(config.fetchConfig("DB"),super_pnr))==config.fetchConfig("paymentGateway1")
		    	||(sqlutil.getPGFromRequestAuditTable(config.fetchConfig("DB"),super_pnr))==config.fetchConfig("paymentGateway2"))
			  {
		    	  subOptionsPayopNameAndResult.put(netBankingOtherOption[y], (sqlutil.getPGFromRequestAuditTable(config.fetchConfig("DB"),super_pnr)));
			  }
		      driver.get(config.fetchConfig("baseUrl"));
			 Thread.sleep(5000);
			  sapg.loginOnSapg(config.fetchConfig("custEmail"), config.fetchConfig("passwordSapg"), 
					  config.fetchConfig("superPNR"));
			 Thread.sleep(5000);
			  payswift.selectNetBankingPayOp(); 
		}
		  for(Entry<String, String> nn : subOptionsPayopNameAndResult.entrySet())
		  {
			  System.out.println("List of Banks which are on ccavenueDirect" + nn.getKey().toString()+" and "+ nn.getValue().toString());
		  }
  }
  
  
  @AfterMethod(groups={"TestAllNBAndWalletOptions"})
  public void afterMtethod(ITestResult testResult, Method m) throws IOException 
  {	  
	 // extentReport.commonAfterMethodForReporting(testResult, driver);
	  if(testResult.getStatus()==2){
		  screenshotAndOrderId.put(super_Pnr,"screenshot attached in mail");
		  testCaseNameAndResult.put(m.getName(), "Fail");
		  test.log(LogStatus.FAIL, m.getName());
		}else if(testResult.getStatus()==1){
			testCaseNameAndResult.put(m.getName(), "Pass");
			 screenshotAndOrderId.put(super_Pnr, "NA as TC passed");
			 test.log(LogStatus.PASS, m.getName());
		}else{
			testCaseNameAndResult.put(testResult.getName().toString(), "Skip");	
			 screenshotAndOrderId.put(super_Pnr,"NA as TC skipped");
			 test.log(LogStatus.SKIP, testResult.getName().toString());
		}
  }
  
  @AfterClass(groups={"TestAllNBAndWalletOptions"})
  public void afterClass() throws SQLException, IOException, InterruptedException {
	
	 for(Entry<String, String> nn : screenshotAndOrderId.entrySet())
	  {
		  System.out.println(nn.getKey().toString()+" and "+ nn.getValue().toString());
	  }
	  ReportGenerator.generateHTMLReportForTestsResults(testCaseNameAndResult, screenshotAndOrderId);
	  Thread.sleep(2000);
	  extentReport.sendMailWithAttachmentAndBodyForTestCaseReport
		(config.fetchConfig("sender"),
	config.fetchConfig("receiver"),"ExtentReportAPI.html", "HTMLReportCommonTable.html","Production", 
	config.fetchConfig("subjectOfReportMail"),config.fetchConfig("bodyOfSubjectMail") , testCaseNameAndResult) ;
	    driver.close();
		driver.quit();
		report.flush();
  }


}
