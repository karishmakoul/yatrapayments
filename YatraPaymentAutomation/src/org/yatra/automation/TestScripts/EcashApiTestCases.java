package org.yatra.automation.TestScripts;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import org.testng.asserts.SoftAssert;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.yatra.automation.ECashApi.GetEcashRequest;
import org.yatra.automation.ECashApi.GetEcashResponse;
import org.yatra.automation.ECashApi.RedeemAuthCallRequestApi;
import org.yatra.automation.ECashApi.RedeemAuthCallResponse;
import org.yatra.automation.Utility.FileUtil;
import org.yatra.automation.Utility.JsonUtil;
import org.yatra.automation.Utility.SQLUtil;
import org.yatra.automation.propertyAndConfig.ConfigProperties;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EcashApiTestCases {

	GetEcashRequest getEcashReq;
	JsonUtil jsonutil;
	Logger logger = LoggerFactory.getLogger(EcashApiTestCases.class);
	HashMap<String, String> mapForParamMapping;
	
	String url= null;
	FileUtil fileUtil;
	SQLUtil sqlUtility;
	ConfigProperties config;
	GetEcashResponse getEcashRes ;
	public static ObjectMapper objectMapper ;
	SoftAssert sAssert ;
	//DataInfoMapping dataMapper= new DataInfoMapping();
	RedeemAuthCallRequestApi redeemAuthCallRequestApi;
	static String[][] arrayString = null;
	RedeemAuthCallResponse redeemAuthCallResponse;
	
	@BeforeClass
	public void BeforeClass()
	{
		jsonutil = new JsonUtil();
		getEcashReq = new GetEcashRequest();		
		sqlUtility = new SQLUtil();
		config= new ConfigProperties("PROD_FILE");
		objectMapper = new ObjectMapper();
		redeemAuthCallRequestApi = new RedeemAuthCallRequestApi();
		sAssert = new SoftAssert();
		getEcashRes = new GetEcashResponse();
		mapForParamMapping= new HashMap<String, String>();
		redeemAuthCallResponse = new RedeemAuthCallResponse();
	}
			
	@Test(description = "Verify Current Ecash from JSON Res")
	public void verifyCurrentEcash() throws JsonParseException, JsonMappingException, IOException
	{	
		mapForParamMapping=getEcashReq.getMapperForGetEcashRequest();
		url= getEcashReq.getApiUrl()+
				jsonutil.convertToQueryParam(mapForParamMapping);
		System.out.println(url);
		getEcashRes= objectMapper.readValue(jsonutil.getApiResponse(url), GetEcashResponse.class);
		if (Integer.parseInt(getEcashRes.getTotalEcashDto().getCurrentECashInPaisa())!=
			(Integer.parseInt(getEcashRes.getEcashListDto().get(0).getCurrentECashInPaisa())+
					Integer.parseInt(getEcashRes.getEcashListDto().get(1).getCurrentECashInPaisa())))
		{
			sAssert.fail("Ecash coming from backend is not correct");
		}
	}
	
	@Test()
	public void verifyAuthCall() throws JsonParseException, JsonMappingException, IOException
	{
		mapForParamMapping = redeemAuthCallRequestApi.getMapperForApi();
		url= redeemAuthCallRequestApi.getApiUrl()+
				jsonutil.convertToQueryParam(mapForParamMapping);
		System.out.println(url);
		redeemAuthCallResponse= objectMapper.readValue(jsonutil.postAPIResponse(url),
				RedeemAuthCallResponse.class);
	}
	
	
	
	@AfterTest()
	public void AfterTest()
	{
		sAssert.assertAll();
		
	}
}
