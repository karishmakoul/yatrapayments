package org.yatra.automation.TestScripts;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.yatra.automation.Utility.FileUtil;
import org.yatra.automation.Utility.SQLUtil;
import org.yatra.automation.Utility.JsonUtil;
import org.yatra.automation.paymentsAPI.PaymentResponse;
import org.yatra.automation.paymentsAPI.paymentRequestAPI;
import org.yatra.automation.paymentsAPI.queryTransactionApiRequest;
import org.yatra.automation.propertyAndConfig.ConfigProperties;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ApiTesting 

{	
	paymentRequestAPI getBookingDetails;
	PaymentResponse paymentResponse;
	queryTransactionApiRequest getQueryTransactiondapi;
	JsonUtil jsonutil;
	Logger logger = LoggerFactory.getLogger(ApiTesting.class);
	HashMap<String, String> mapForParamMapping= new HashMap<String, String>();
	HashMap<String, String> mapForParamMappingRefundAPI= new HashMap<String, String>();
	String url= null;
	FileUtil fileUtil;
	SQLUtil sqlUtility;
	ConfigProperties config;
	public static ObjectMapper objectMapper ;
	@BeforeClass
	public void BeforeMethod()
	{
		jsonutil = new JsonUtil();
		getBookingDetails = new paymentRequestAPI();
		getQueryTransactiondapi= new queryTransactionApiRequest();
		fileUtil= new FileUtil();
		sqlUtility = new SQLUtil();
		config= new ConfigProperties("PROD_FILE");
		objectMapper = new ObjectMapper();
		paymentResponse = new PaymentResponse();
	}
	
	@Test(description = "get card type from getPaymentInfo request")
	public void getCardDetailsFromgetPaymentInfo() throws JSONException, JsonParseException, JsonMappingException, IOException
	{
		mapForParamMapping=getBookingDetails.getMapperForPaymentRequest();
		url= getBookingDetails.getApiUrl()+
				jsonutil.convertToQueryParam(mapForParamMapping);
		String res=jsonutil.postAPIResponse(url);
		System.out.println("URL is : " + res);
		//jsonutil.fetchKeyValuePair(res);
		paymentResponse= objectMapper.readValue(res, PaymentResponse.class);
	    String str=paymentResponse.getStoredCardJson().getSavedCard();
        System.out.println(str);
	}
	
	@Test(description="Get status of QueryTransaction API")
	public void getStatusOfQueryTransactionAPI() throws UnsupportedEncodingException
	{
		mapForParamMappingRefundAPI= getQueryTransactiondapi.getMapperForQueryTransactionAPI();
		url= getQueryTransactiondapi.getApiUrl()+
				jsonutil.convertToQueryParam(mapForParamMappingRefundAPI);
		String res=jsonutil.getApiResponse(url);
		System.out.println("URL is : " + res);
		System.out.println(fileUtil.parsePipeSeparatedStringRes("desc", res));
	}
	
	
	

}
