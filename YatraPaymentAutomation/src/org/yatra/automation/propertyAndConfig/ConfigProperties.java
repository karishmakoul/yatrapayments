package org.yatra.automation.propertyAndConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

public class ConfigProperties
{
	private static final String pathAppender = "/src/org/yatra/automation/propertyAndConfig/";
	//private static final String extension = ".properties";
	private String environment;
	
	//This contructor gets invoke in every test script class
	public ConfigProperties(String environment) 
	{
		this.environment = environment;
	}
	public String fetchConfig(String property) 
	{
		String result = null;
		InputStream inputStream = null;
		
		try
		{
			Properties prop = new Properties();
			inputStream = new FileInputStream(System.getProperty("user.dir")+
					pathAppender + environment);
			prop.load(inputStream);
			System.out.println(inputStream);
			Date time = new Date(System.currentTimeMillis());
			System.out.println(time);
			result = prop.getProperty(property);
			System.out.println("Config, " + property + " = " + result + ", fetched @ " + time);
		}
		catch(Exception e)
		{
			System.out.println("Exception: " + e);
		}
		finally
		{
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;

	}
	
	
}
