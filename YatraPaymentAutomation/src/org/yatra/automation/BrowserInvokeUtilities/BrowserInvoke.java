package org.yatra.automation.BrowserInvokeUtilities;

import java.io.File;
import java.util.logging.Level;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;


public class BrowserInvoke {
 
	private static WebDriver driver;
	private static String OS = System.getProperty("os.name").toLowerCase();
	
	public static WebDriver invokeBrowser(String browser)
	{
		File file = new File(System.getProperty("user.dir") +
				System.getProperty("file.separator")+"drivers");
		
		String browser_driver_path =null;
		if (file.exists())
		{
			browser_driver_path = System.getProperty("user.dir") + 
					System.getProperty("file.separator")+"drivers"+
					System.getProperty("file.separator");
		
		    System.out.println(browser_driver_path);
		}   
		else
			Assert.fail("Drivers not found exception");
		
		if (browser.equalsIgnoreCase("firefox")) 
		{
			System.setProperty("webdriver.gecko.driver", 
					"E:\\GIT\\yatrapayments\\YatraPaymentAutomation\\drivers\\geckodriver.exe");
			DesiredCapabilities dc = new DesiredCapabilities();
			dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			ProfilesIni allProfiles = new ProfilesIni();
			FirefoxProfile profile = allProfiles.getProfile("firefox_profile");
		//profile.setPreference("extensions.checkCompatibility", false);
			profile.setAcceptUntrustedCertificates(true);
			profile.setAssumeUntrustedCertificateIssuer(false);
			dc.setCapability(FirefoxDriver.PROFILE, profile);
			driver = new FirefoxDriver(dc);
			driver.manage().window().maximize();
		}
		else if(browser.equalsIgnoreCase("chrome"))
		{
			if (OS.indexOf("win") >= 0)
				System.setProperty("webdriver.chrome.driver", browser_driver_path + "chromedriver.exe");
			if (OS.indexOf("mac") >= 0)
				System.setProperty("webdriver.chrome.driver", browser_driver_path + "chromedriver");
			
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true);
			LoggingPreferences loggingprefs = new LoggingPreferences();
			loggingprefs.enable(LogType.BROWSER, Level.ALL);
			capabilities.setCapability(CapabilityType.LOGGING_PREFS,loggingprefs);
			capabilities.setCapability("chrome.switches", "--incognito");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("able-popup-blocking");
			options.addArguments("--start-maximized");
			capabilities.setCapability(ChromeOptions.CAPABILITY,options);
			options.setBinary("E:\\chrome64_57.0.2987.133\\chrome.exe");
			//options.addArguments("--user-agent=Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
			driver = new ChromeDriver(options);
			
		}
		 else if (browser.equalsIgnoreCase("ie"))
		   {
			 
			    System.out.println("Path of  IE driver is: " +  browser_driver_path  );
				System.setProperty("webdriver.ie.driver", browser_driver_path + "IEDriverServer.exe");
				DesiredCapabilities caps = new DesiredCapabilities();
				caps.setCapability(InternetExplorerDriver.LOG_LEVEL, "DEBUG");
				caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						 true);
				driver = new InternetExplorerDriver();
				driver.manage().window().maximize();
			} 
		 else if (browser.equalsIgnoreCase("safari"))
		    {
				driver = new SafariDriver();
				driver.manage().window().maximize();
			}
		
			return driver;
	}
}
