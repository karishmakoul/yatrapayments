package org.yatra.automation.MOUtils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MO_ObjectRepository {
	WebDriver driver ;
	
	public MO_ObjectRepository(WebDriver driver) {
		this.driver = driver;
	}

	public MO_ObjectRepository pageObject() {
		return PageFactory.initElements(driver, MO_ObjectRepository.class);
	}
	
//************************Login Elements*************************
	@FindBy(xpath="//input[@name='username']")
	private WebElement username;
	
	@FindBy(xpath="//input[@name='password']")
	private WebElement password;
	
	@FindBy(xpath="//a[@href='javascript:validate(document.form)']")
	private WebElement login;
	
//***************Booking ref id******************************
	@FindBy(xpath="//input[@name='txtbookingref']")
	private WebElement 	bookingRefNo;
	
	@FindBy(xpath="//button[@name='search']")
	private WebElement searchBookingRefNo;
	
//*************************Click on Payment Button*********************************
	@FindBy(id="payfont")
	private WebElement paymentOtpion;

//*************************Recieve payment**************************************	
	@FindBy(xpath="//td/a[@href='javascript:winopen('allocate_payment.asp?booking_ref=16081044',500)']")
	private WebElement receivePayment;
//******************************************************************	
	
	@FindBy(xpath="//tbody//tr[3]/td[2]/select")
	private WebElement selectModeOfPayment;
	
	
}
