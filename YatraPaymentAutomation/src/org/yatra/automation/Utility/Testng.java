package org.yatra.automation.Utility;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.testng.TestNG;

public class Testng {

	static List<XmlClass> myClasses = null;
	Logger logger = LoggerFactory.getLogger(Testng.class);
	
	public static void testNgXmlCreation(String environment, String tests,
			String browser, String groups)
	{
		String packageName=FileUtil.getConstantValue("TestPackage");
		TestResultListener testresult = new TestResultListener();
		ReportGenerator extentReporterNG = new ReportGenerator();
		TestNG testNG = new TestNG();
        testNG.addListener(testresult);
		testNG.addListener(extentReporterNG);
		
		//Initialize the suite Name
		XmlSuite mySuite = new XmlSuite();
		mySuite.setName(FileUtil.getConstantValue("SuiteName"));
		
		String path = null;
		List<String> packageClasses;
		
		String[] testArray = tests.split(",");
		String browserArray[] = browser.split(",");
		XmlTest myTest = null;
		// List<XmlClass> myClasses = null;
		List<XmlPackage> myPackages = null;
		List<XmlTest> myTests = new ArrayList<XmlTest>();
		
	}
}
