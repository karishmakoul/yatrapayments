package org.yatra.automation.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

public class FileUtil {
	
	public static String fileSeparator= System.getProperty("file.separator");
	private static String fileName= fileSeparator + "Constant.cfg";
	private static InputStream input = null;
	private String[] listStrs;
	
	public static String getConstantValue(String property)
	{
		Properties prop =  new Properties();
		String value = null;
		String path = null;
		System.out.println("File name is : " +System.getProperty("user.dir")+fileSeparator+fileName );
		try
		{
			path = System.getProperty("user.dir") + fileSeparator + fileName;
			
			File f = new File(path);
			if (f.exists()) 
			{
				input = new FileInputStream(path);
			} 
			prop.load(input);
			value = prop.getProperty(property);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}
	
	/**
	 * @author karishma.koul
	 * @param keyword to fetch from pipe separated response.
	 * @param actual pipe separated response
	 * @return Keyword to be serached
	 */
	
	public String parsePipeSeparatedStringRes(String keywordToFetch, String actualString)
	{
		String str =null;
		String[] tokens = actualString.split("\\|");
		for (String token : tokens)
		{
		    System.out.println(token);
		    if(token.contains("desc"))
		    {
		    	str= token.split(":")[1];
		    	System.out.println(str);
		    	 break;
		    }
		}
		return str;		
	}
	
	/**
	 *  
	 */
	
	public String[] parseCommaSeparatedString(String commaSeparatedString)
	{
		  listStrs = null;
		  if(commaSeparatedString.contains(","))
		   {
			    listStrs= commaSeparatedString.split(","); 
			   return listStrs;
		   }
		   else
		   {
			   	 listStrs[0]= commaSeparatedString;
		         return listStrs;
		   }
	}
	
	public String returnSuperPnrFromTtid(String str)
	{
		return str.substring(2);
	}
	
	public void printHashMAp(HashMap<String, String> hm)
	{
		Iterator it = hm.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        System.out.println(pair.getKey() + " = " + pair.getValue());
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	}

}
