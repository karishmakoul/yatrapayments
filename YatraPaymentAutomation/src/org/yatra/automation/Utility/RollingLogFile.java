package org.yatra.automation.Utility;
import java.util.logging.Logger;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class RollingLogFile {      
     

    static{
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        System.setProperty("current.date.time", dateFormat.format(new Date()));
    }
        public static void createLoggerTextFile(String className, String strToBeLogged){
        Logger logger = Logger.getLogger(className);
        
        try {
            //
            // Creating an instance of FileHandler with 5 logging files
            // sequences.
            //
            FileHandler handler = new FileHandler("default.log", true);
            handler.setFormatter(new SimpleFormatter());
            logger.setLevel(Level.ALL);
            logger.addHandler(handler);
            logger.setUseParentHandlers(false);
        } catch (IOException e) {
            logger.warning("Failed to initialize logger handler.");
        }
        logger.info(strToBeLogged);
       
    }
}