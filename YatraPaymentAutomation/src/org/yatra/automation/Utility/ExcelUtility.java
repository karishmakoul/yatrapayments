package org.yatra.automation.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.yatra.automation.FetchPaymentDetailUtility.ConstantUtil;

public class ExcelUtility {
	
	static Workbook  workBook = null;
	static FileInputStream inputStream;
//	static Sheet sheet;
	String [] arr= new String[2];
	
	public Workbook getWorkbook() throws Exception
	{	
		File file = new File(ConstantUtil.filePath+ File.separator + ConstantUtil.fileName);
		inputStream = new FileInputStream(file);	
		
		String fileExtensionName = ConstantUtil.fileName.substring(ConstantUtil.fileName.indexOf("."));
	    if(fileExtensionName.equals(".xlsx"))
		    {
		    	workBook = new XSSFWorkbook(inputStream);	 	  
		    }
		    else if(fileExtensionName.equals(".xls"))
		    {   	 
		    	workBook = new HSSFWorkbook(inputStream);
		    }
		  return workBook;
	}
	
	/**
	 * 
	 * @param counter -- index of row
	 * @param productNameCol - name of column
	 * @param paymentGatewayCol -- name of column
	 * @return String array with values in both column
	 * @throws Exception
	 */
	
	public String[] readxlsfile( int counter, int...colNum  ) throws Exception
	{					  	  
		for(int i=0;i<colNum.length;i++)
		{
	    arr[i]=getWorkbook().getSheet(ConstantUtil.sheetName).getRow(counter).getCell(colNum[i]).getStringCellValue();
	 	//arr[1]= getWorkbook().getSheet(ConstantUtil.sheetName).getRow(counter).getCell(colNum[1]).getStringCellValue();
		}
	 	return arr;	
	}
	
	public int getTotalRowInSheet() throws Exception
	{
		return getWorkbook().getSheet(ConstantUtil.sheetName).getPhysicalNumberOfRows()-1;
	}
	
	
	
	  
}	
	

