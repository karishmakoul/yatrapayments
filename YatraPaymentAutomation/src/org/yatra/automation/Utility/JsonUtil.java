package org.yatra.automation.Utility;

import static com.jayway.restassured.RestAssured.given;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Scanner;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.yatra.automation.ECashApi.GetEcashRequest;
import org.yatra.automation.ECashApi.GetEcashResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class JsonUtil {

	public static ObjectMapper objectMapper = new ObjectMapper();
	String res;
	JSONArray jsonArr = null;
	RollingLogFile logger= new RollingLogFile();
	GetEcashRequest getEcashReq = new GetEcashRequest();
	GetEcashResponse getEcashRes = new GetEcashResponse();
	
	public String convertToQueryParam(HashMap <String, String> mapForParamMapping) throws UnsupportedEncodingException
	{
		StringBuffer queryParam= new StringBuffer();
		queryParam.append("?");
		String ampersand = "";
		if(mapForParamMapping==null || 
				mapForParamMapping.isEmpty() )
		{
			return null;
		}
		
		for(String key : mapForParamMapping.keySet())
		{
			System.out.println(key);
			String value = mapForParamMapping.get(key);
			if((value!=null)&&(value.length()>0))
			{
				queryParam.append(ampersand);
				//append the parameter
				if(value.contains("@"))
				{
					queryParam.append(key);
					queryParam.append("=");
					queryParam.append(value);
				}
				else
				{
					 queryParam.append(URLEncoder.encode(key,"UTF-8"));
					 queryParam.append("=");
					 queryParam.append(URLEncoder.encode(value,"UTF-8"));
				}
				
				
			}
			ampersand="&";
		}
		
		return queryParam.toString();
	}
	
	public  String postAPIResponse(String api) {
    	
    	RequestSpecBuilder builder = new RequestSpecBuilder();    	
    	builder.setContentType("application/json; charset=UTF-8");      	 
    	RequestSpecification requestSpec = builder.build();    	    	
    	Response response = given().authentication().preemptive().basic("", "")
    			.spec(requestSpec).when().post(api);    	
    	
    	 res= response.body().asString();
    	return res;    	
	}
	
	public String getApiResponse(String api)
	{
		try {
			URL url = new URL(api);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
            System.out.println("Response code is :" + conn.getResponseCode());
			if (conn.getResponseCode() != 200 ) 
			{
				System.out.println(conn.getResponseCode());
			    throw new RuntimeException(" HTTP error code : " + conn.getResponseCode());
			}
			
			
			Scanner scan = new Scanner(url.openStream());
			String entireResponse = new String();
			while (scan.hasNext())
			{
				entireResponse += scan.nextLine();
				System.out.println(entireResponse);
			}
			System.out.println("Response : " +entireResponse);

			scan.close();
			return entireResponse;
		}catch(Exception ex)
		{
			return null;
		}
    
	}

	public void fetchKeyValuePair(String JsonString, Object responseClassName) throws JSONException, JsonParseException, JsonMappingException, IOException
	{
		/*paymentResponse paymentResp = objectMapper.readValue(JsonString, paymentResponse.class);
		getEcashRes= objectMapper.readValue(JsonString, GetEcashResponse.class);
	    String str=paymentResp.getStoredCardJson().getSavedCard();
        System.out.println(str);*/
      
 		/*JsonObject obj= new JsonObject();
		for(int i=0; i<JsonString.length(); i++)
		   {			 
			 System.out.println(jsonArr.getJSONObject(i).getString(ObjectName));
			 
			 if(ObjectName.equalsIgnoreCase(jsonArr.getJSONObject(i).getString(ObjectName)))
			 {
			    //jsonArr = new JSONArray(JsonString);				 
		        JSONObject jsonObj  = jsonArr.getJSONObject(i);
		        System.out.println(jsonObj.getString(ObjectName));
		 	 }   
		   }*/
	}
	
	public String getHttpsApiResponse(String api)
	{
		
		  URL url;
	      try {

		     url = new URL(api);
		     HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		     con.setHostnameVerifier(new HostnameVerifier()
		     {      
		    	    public boolean verify(String hostname, SSLSession session)
		    	    {
		    	        return true;
		    	    }
		    	});

		//     print_https_cert(con);

		     //dump all the content
		 // return   print_content(con);
		     return null;

	      } catch (MalformedURLException e) {
		     e.printStackTrace();
		     return   null;
	      } catch (IOException e) {
		     e.printStackTrace();
		     return   null;
	      }
	}
	
	/* private void print_https_cert(HttpsURLConnection con){

		    if(con!=null){

		      try {

			System.out.println("Response Code : " + con.getResponseCode());
			System.out.println("Cipher Suite : " + con.getCipherSuite());
			System.out.println("\n");

			Certificate[] certs = con.getServerCertificates();
			for(Certificate cert : certs){
			   System.out.println("Cert Type : " + cert.getType());
			   System.out.println("Cert Hash Code : " + cert.hashCode());
			   System.out.println("Cert Public Key Algorithm : "
		                                    + cert.getPublicKey().getAlgorithm());
			   System.out.println("Cert Public Key Format : "
		                                    + cert.getPublicKey().getFormat());
			   System.out.println("\n");
			}

			} catch (SSLPeerUnverifiedException e) {
				e.printStackTrace();
			} catch (IOException e){
				e.printStackTrace();
			}

		     }

		   }*/
	 
   /*private String print_content(HttpsURLConnection con) throws IOException{
	 String input= null;
		if(con!=null){			
	    System.out.println("****** Content of the URL ********");
			  
	    BufferedReader br =
				new BufferedReader(
					new InputStreamReader(con.getInputStream()));	   

	     while ((input = br.readLine()) != null)
	     {
	    	 RollingLogFile.createLoggerTextFile(this.getClass().getSimpleName(), input);
		      System.out.println(input);
		  }
			   
			    br.close();
     }
		 return input;
		   }*/

	
    public void parsingJsonArray(JSONObject jsonObject) throws JSONException
    {
		 JSONArray array = jsonObject.getJSONArray("ECashList");
		 System.out.println(array.length());
    }
	
	public void parseListofObject()
	{
		
	}
	
}
