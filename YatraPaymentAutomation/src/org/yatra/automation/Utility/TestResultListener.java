package org.yatra.automation.Utility;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;


public class TestResultListener extends TestListenerAdapter {
	int c =1;

    @Override
    public void onTestFailure(ITestResult result) {
    	System.out.println("Finished Test: "+result.getName()+" :FAILED");
        //WebDriverOperations.createAttachment();
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Finished Test: "+result.getName()+" :PASSED");
        //WebDriverOperations.createAttachment();
    }
    @Override
    public void onTestStart(ITestResult result) {
       // System.out.println("Started Test: "+result.getName());
    }
    
    @Override
    public void onTestSkipped(ITestResult result) {
       // System.out.println("Finished Test: "+result.getName()+" :SKIPPED");
    }
    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
       // System.out.println("Finished Test: "+result.getName()+" :FAILED BUT WITHIN SUCCESS PERCENTAGE");
    }
    @Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
	}
    @Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
	}
}
