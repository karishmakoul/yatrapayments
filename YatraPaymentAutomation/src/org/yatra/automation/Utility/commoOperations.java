package org.yatra.automation.Utility;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class commoOperations 
{
	 Alert simpleAlert;
   public void waitTillElementNotfound(WebElement el, WebDriver driver)
   {
		 Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				   .withTimeout(30, TimeUnit.SECONDS)
				   .pollingEvery(5, TimeUnit.SECONDS)
				   .ignoring(NoSuchElementException.class);
		 
	 apply(el,  driver);
	   
   }
   
   public WebElement apply(WebElement el, WebDriver driver)
   {
	  return el;
   }
   
   /**
    * @param driver
    * @description Waits for Page to Load completely
    */
   
   public void waitForPageLoad( WebDriver driver)
   {
//	   ExpectedCondition<Boolean> expectation = new
//               ExpectedCondition<Boolean>() {
//                   public Boolean apply(WebDriver driver) {
//                       return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
//                   }
//               };
//       WebDriverWait wait = new WebDriverWait(driver, 50);
//       wait.until(expectation);
      /* try {
           Thread.sleep(3000);
           WebDriverWait wait = new WebDriverWait(driver, 50);
           wait.until(expectation);
       } catch (Throwable error) {
           Assert.fail("Timeout waiting for Page Load Request to complete.");
       }*/
   }
   
   public void acceptAlert(WebDriver driver)
   {
	   try
	   {
	   simpleAlert = driver.switchTo().alert();
	   simpleAlert.accept();
	   }
	   catch(Exception ex){}
   }
   
   public void rejectAlert(WebDriver driver)
   {
	   simpleAlert = driver.switchTo().alert();
	   String alertText = simpleAlert.getText();
	   System.out.println("Alert text is " + alertText);
	   simpleAlert.dismiss();
   }
   
  /**
   * @param : String to convert into integer.
   * @description: Takes a string and if string contains ',' 
   * then it returns the integer without comma.
   * @param str
   * @return an integer
   * @author karishma.koul
   */
   public int convertStringToInt(String str)
   {
	   if(str.contains(","))
	   {
		   String[] listStr= str.split(","); 
		   return Integer.parseInt((listStr[0]+listStr[1]));
	   }
	   else
	   return  Integer.parseInt(str);
   }
   
   /**
    * @param driver
    * @return a String
    * @author karishma.koul
    * @description Takes super pnr from js file
    */
   public String getSuperPnr(WebDriver driver)
   {
	  try
	  {
	   JavascriptExecutor js = (JavascriptExecutor) driver;
		return (String)js.executeScript("return detailsJson.superPnr");
	  }
	  catch(Exception ex)
	  {
		  return "no super_Pnr";
	  }
   }
   
   /**
    * @param WebElement
    * @param driver
    */
   
   public void scrollTillElementIsVisible(WebElement ele, WebDriver driver)
   {
	   JavascriptExecutor je = (JavascriptExecutor) driver;
	   je.executeScript("arguments[0].scrollIntoView(true);",ele);
   }
   
   /**
    * @param driver
    * @param webElement
    */
   public void waitTillElementPresent(WebDriver driver, WebElement el )
   {   
	   try{
	    Thread.sleep(5000);
	    JavascriptExecutor javascript = (JavascriptExecutor) driver;
	    javascript.executeScript("arguments[0].setAttribute('style', arguments[1]);", el, "color: orange; border: 4px solid orange;");
	    javascript.executeScript("arguments[0].setAttribute('style', arguments[1]);", el, "");
	    
	   }catch(Exception ex){ }
	    
   }

}
