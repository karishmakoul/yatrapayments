package org.yatra.automation.Utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.yatra.automation.propertyAndConfig.ConfigProperties;

public class SQLUtil {
	
	public static Connection connection;
	static ConfigProperties config= new ConfigProperties("PROD_FILE");
	LinkedList<String> list	=	new LinkedList<String>();
	PreparedStatement queryToGetSuperPnr = null; 
	ResultSet resultSet = null;
	LinkedHashMap<String, String> mapperPropNameAndPropValue = new LinkedHashMap<String, String>();
	
	public Connection  sqlConnectionwithDBNameEx(String DBname) throws Exception 
	 {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			System.out.println(config.fetchConfig("DBUsername")+ " "+ config.fetchConfig("DBPassword"));
			connection = DriverManager.getConnection("jdbc:mysql://"+config.fetchConfig("DBIP") +":"+ 
			config.fetchConfig("ForwardPort") + "/" + DBname,config.fetchConfig("DBUsername"),
			config.fetchConfig("DBPassword"));		
			System.out.println("SQL DB connection created successfully !!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
		
	 }
	
	public   List<String> executeQueryEmptySet(String query) throws SQLException
	{		
		ResultSet resultSet = null;
       	try 
		{   Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while(resultSet.next())
			{
			System.out.println(resultSet.getString(1));
		    list.add(resultSet.getString(1));
			}
				
		} catch (SQLException e) {
                  e.printStackTrace();
		}
       	finally
       	{
       		connection.close();
       	}
		return list;
	}
	
	
	public List<String> getGatewayNameForABank(String DB, String bankCodeSelected, String gateway) throws Exception
	{
		String queryForGettingPrimaryPG="select gateway_code from yp_payment_gateway_master where id in "+
				 "( select "+ gateway +" from yp_payment_banks_master where code = "+"'"
				 +bankCodeSelected+"' );";
			
				 
		sqlConnectionwithDBNameEx(DB);
		return executeQueryEmptySet(queryForGettingPrimaryPG);
		
	}
	
	public String getPGFromRequestAuditTable(String DB, String superPnr) throws Exception
	{
		
		String queryForGettingPGinReqTable = "select payment_gateway from payment_request_audit where "
		  		+ "super_pnr_code = " +"'"+superPnr+"'" + " order by timestamp desc limit 1;";		
		sqlConnectionwithDBNameEx(DB);
		List<String> pg= executeQueryEmptySet(queryForGettingPGinReqTable);
		System.out.println(pg.get(0));
		return pg.get(0);
		
	}
	
	
	
	@SuppressWarnings("finally")
	public LinkedHashMap<String, String> getPgMapperProperties(String DB, String product_code, String PaymentGateway) throws Exception
	{
		   
		sqlConnectionwithDBNameEx(DB);		
		  String selectMapperProperties ="select property_key, property_value from merchant_pg_mapper_properties where merchant_pg_key in (select merchant_pg_key "
				  +"from merchant_pg_mapper where payment_gateway = ? and product_code= ?) ;"  ;
		  try {
			   queryToGetSuperPnr = connection.prepareStatement(selectMapperProperties);		      
			   queryToGetSuperPnr.setString(1, PaymentGateway);
			   queryToGetSuperPnr.setString(2, product_code);
			   resultSet= queryToGetSuperPnr.executeQuery();			   
			   System.out.println(resultSet.getFetchSize());
			   
			   while(resultSet.next())
				{
				  
				   mapperPropNameAndPropValue.put(resultSet.getString("property_key"), 
						   resultSet.getString("property_value"));
				}
			   return mapperPropNameAndPropValue;
		  } catch (SQLException e ) {
		         if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		                return mapperPropNameAndPropValue;
		            } catch(SQLException excep) {
		                return mapperPropNameAndPropValue;
		            }
		        }
		    } finally {
		        if (queryToGetSuperPnr != null) 
		        	queryToGetSuperPnr.close();		        	
		        
		       		connection.close();
		       	 return mapperPropNameAndPropValue;
		        
		      }
 
	}
	
	@SuppressWarnings("finally")
	public LinkedList<String> getTopFiveSuperPnrByLobNameAndPG (String DB, String str1, String str2, String str3) throws Exception
	{
		  sqlConnectionwithDBNameEx(DB);	
		 
		  String selectString =
			        "select super_pnr_code from payment_request_audit where ui_product = ?"
			        + " and payment_gateway in (?,?) order by timestamp desc limit 5;"; 
		  
		  try {
			   queryToGetSuperPnr = connection.prepareStatement(selectString);		      
			   queryToGetSuperPnr.setString(1, str1);
			   queryToGetSuperPnr.setString(2, str2);
			   queryToGetSuperPnr.setString(3, str3);
			   resultSet= queryToGetSuperPnr.executeQuery();
			   while(resultSet.next())
				{
				System.out.println(resultSet.getString(1));
			    list.add(resultSet.getString(1));
				}
			   return list;
		       
		    } catch (SQLException e ) {
		         if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		                return list;
		            } catch(SQLException excep) {
		                return list;
		            }
		        }
		    } finally {
		        if (queryToGetSuperPnr != null) 
		        	queryToGetSuperPnr.close();		        	
		        
		       		connection.close();
		       	 return list;
		        
		      }
	}

}
