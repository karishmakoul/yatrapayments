package org.yatra.automation.Utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.IClass;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ReportGenerator {

	ExtentReports extent;
	ExtentTest test;
	public static String separator = System.getProperty("file.separator");
	static int count = 0;
	public ExtentReports startInstance()
	{
		extent=new ExtentReports(System.getProperty("user.dir")+"/test-output/PaymentAutomationReport.html" , true);
		extent.addSystemInfo("Enviorment", "PROD").addSystemInfo("Reporter Name", "Karishma Koul");
		extent.loadConfig(new File(System.getProperty("user.dir")+"/extent-config.xml"));
		return extent;
	}
	
	public void commonAfterMethodForReporting(ITestResult testResult, WebDriver driver) throws IOException 
	{
		 File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	 	 FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") +
		 separator + "test-output"+ separator +"FilesForReporting"+ separator+ testResult.getName()+".png"));	 
	 	 
	 	 
	}

	public static void generateHTMLReportForTestsResults(LinkedHashMap<String, String> testCaseNameAndResult,
			LinkedHashMap<String, String> screenshotAndOrderIdMap) throws IOException {
		FileWriter f;
		f = new FileWriter(System.getProperty("user.dir") + "/HTMLReportCommonTable.html", false);
		BufferedWriter bw = new BufferedWriter(f);
		bw.write("<body>");
		bw.write("<h3 style=\"color:black\" align=\"center\"><b> Test Case Result Report </b></h3>");
		bw.write(
				"<TABLE BORDER='2' CELLPADDING=0 CELLSPACING=1 align=\"center\" bordercolor=\"green\" style=\"width:90%\">");
		bw.write(
				"<tr>" + "<th align=\"center\" width=\"40%\"><h5 style=\"color:blue\" align=\"center\"><b> Test Case Name </b></h5></td>"
						+ "<th align=\"center\" width=\"20%\"><h5 style=\"color:blue\" align=\"center\"><b> Test Case Result </b></h5></td>"
						+ "<th align=\"center\" width=\"20%\"><h5 style=\"color:blue\" align=\"center\"><b> Super Pnr </b></h5></td>"
						+ "<th align=\"center\" width=\"20%\"><h5 style=\"color:blue\" align=\"center\"><b> Screenshot If Fail </b></h5></td>"
						+ "</tr>");
		
		for (Entry<String, String> m : testCaseNameAndResult.entrySet()) {
			
			System.out.println("Test case name and result "+m.getKey().toString()+" and "+ m.getValue().toString());
			
			bw.write("<tr>" + "<td align=\"center\" valign=\"top\" style=\"width:40%\">" + m.getKey().toString()
					+ "</td>" + "<td align=\"center\" valign=\"top\" style=\"width:20%\">" + m.getValue().toString()
					+ "</td>");
		
			System.out.println("Count:- "+count);
			int j = 0;
			for(Entry<String, String> n : screenshotAndOrderIdMap.entrySet()) {
				if(j==count)
				{
					    System.out.println("URL of orde id and screenshot : " +n.getKey().toString()+" "+n.getValue().toString());
				        bw.write("<td align=\"center\" valign=\"top\" style=\"width:20%\">" + n.getKey().toString()
						+ "</td>" + "<td align=\"center\" valign=\"top\" style=\"width:20%\">" + n.getValue().toString()
						+ "</td>");
				
				}
						
				j++;
			}
			count++;
			bw.write("</tr>");
		}
		
		
		bw.write("");
		bw.write("</TABLE>");
		bw.write("</body>");
		bw.write("</html>");
		bw.close();
		f.close();
	}
	
public void sendMailWithAttachmentAndBodyForTestCaseReport(String sender, String receiver, String filePath, String filepath2,
			String Environment, String title, String MessageBody, LinkedHashMap<String, String> mapForTestCases)
					throws IOException {
		
		System.out.println("It Has Entered sendMail method.");
		String recipient = receiver;
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "587");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("yatratestbookings@gmail.com", "yatra123");
			}
		});
		try {
			Message message = new MimeMessage(session);
			InternetAddress internetAddress = new InternetAddress("yatratestbookings@gmail.com", "Yatra Automation");
			message.setFrom(internetAddress);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
			message.setSubject(title);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();
			BodyPart messageBodyPart1 = new MimeBodyPart();

			messageBodyPart.setText(" " + MessageBody);
			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			String filename, filename2;
			filename = System.getProperty("user.dir") + "/" + filepath2;
			filename2 = System.getProperty("user.dir") + "/" + filepath2;
			//Started pasting message as body of mail
				// Send the complete message parts
			message.setContent(multipart);

			String mess = "";
			try {
					mess = FileUtils.readFileToString(new File(filename));
			 	} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				messageBodyPart1.setContent(mess, "text/html; charset=utf-8");
				multipart.addBodyPart(messageBodyPart1);
            //Ended pasting message as body of mail
			// 4) create new MimeBodyPart object and set DataHandler object to
		    // this object
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();

			DataSource source = new FileDataSource(filename2);
			messageBodyPart2.setDataHandler(new DataHandler(source));
			messageBodyPart2.setFileName(filename2);
			// Add Images
			for (Entry<String, String> m : mapForTestCases.entrySet()) {
				MimeBodyPart messageBodyPart3 = new MimeBodyPart();
				System.out.println("I am here: -" + m.getKey());
				if(m.getValue().toString().equalsIgnoreCase("FAIL"))
				{
					messageBodyPart3.attachFile(
				    System.getProperty("user.dir") + "/test-output/FilesForReporting/" + m.getKey() + ".png");
					messageBodyPart3.setContentID("<" + System.getProperty("user.dir") + "/test-output/FilesForReporting/"
							+ m.getKey() + ".png" + ">");
				    multipart.addBodyPart(messageBodyPart3);
				}
			}
			
			multipart.addBodyPart(messageBodyPart2);
			
			message.setContent(multipart);
			
	      	Transport.send(message);
	     	System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
